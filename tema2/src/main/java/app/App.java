package app;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.github.tsohr.JSONObject;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.impl.AMQBasicProperties;

public class App {

    public static ArrayList<MonitoredData> read()
    {
        String f="activity.txt";
        try (Stream<String> stream = Files.lines(Paths.get(f)))
        {
            Function<String[],MonitoredData> factory = read->{
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try{
                    String start = Objects.toString(format.parse(read[0]).getTime());
                    String end = Objects.toString(format.parse(read[1]).getTime());
                    return new MonitoredData(format.parse(read[0]).getTime(), format.parse(read[1]).getTime(),read[2]);
                }catch(ParseException e){
                    throw new IllegalArgumentException(e);
                }
            };
            return stream.map(line -> line.split("\t\t")).map(factory::apply)
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }
    private final static String QUEUE_NAME = "mimi";

    public static void main (String[] args) throws Exception{
        ArrayList<MonitoredData> m = read();
        //System.out.println(m);


        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "{\n" +
                    "\"patient_id\": \"3\",\n" +
                    "\"activity\" : \"Sleeping\",\n" +
                    " \"startTime\" : 1570654800000,\n" +
                    " \"endTime\" : 1570654860000\n" +
                    "}";
            Map<String,Object> headerMap = new HashMap<String,   Object>();
            headerMap.put("key1", "value1");
            headerMap.put("key2", new Integer(50) );
            headerMap.put("key3", new Boolean(false));
            headerMap.put("key4", "value4");

            AMQP.BasicProperties messageProperties = new AMQP.BasicProperties.Builder()
                    .timestamp(new Date())
                    .contentType("application/json")
                    .userId("guest")
                    .appId("app id: 20")
                    .deliveryMode(1)
                    .priority(1)
                    .headers(headerMap)
                    .clusterId("cluster id: 1")
                    .build();

            for(int i=0;i<m.size();i++){
                if (i%10==0)
                {
                    Thread.sleep(2000);
                }
              //  System.out.println("10"+m.get(i));
/*
                String messageToBeSent = "{\n" +
                        "\"patient_id\": \"3\",\n" +
                        "\"activity\" : \""+m.get(i).getActivity()+"\",\n" +
                        " \"startTime\" : "+m.get(i).getStart()+",\n" +
                        " \"endTime\" : "+ m.get(i).getEnd()+"\n" +
                        "}";
*/
                String messageToBeSent = new JSONObject().put("patient_id","3").put("activity",m.get(i).getActivity()).put("startTime",m.get(i).getStart()).put("endTime",m.get(i).getEnd()).toString();
                //System.out.println(messageToBeSent);
                channel.basicPublish("", QUEUE_NAME, messageProperties , messageToBeSent.getBytes());
            }


            channel.basicPublish("", QUEUE_NAME, messageProperties , message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");

        }


    }
}
