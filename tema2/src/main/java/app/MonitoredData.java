package app;



import java.sql.Timestamp;
import java.util.Date;


public class MonitoredData {
    private Long start;
    private Long end;
    private String activity;

    public MonitoredData() {
    }

    public MonitoredData (Long start, Long end, String activity)
    {
        this.start=start;
        this.end=end;
        this.activity=activity;

    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String toString(){
        return "start: "+ this.start+ "end: "+ this.end + "activity: "+this.activity+"\n";
    }
}
