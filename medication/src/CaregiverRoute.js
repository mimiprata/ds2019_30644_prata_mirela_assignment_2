import React from 'react';
import {Redirect, Route} from "react-router-dom";

export const CaregiverRoute =({component: Component, ...rest}) =>(
    <Route
        {...rest}
        render={props=>
            localStorage.getItem('role')==='CAREGIVER'?
                (<Component {...props}/>):
                (<Redirect to={{pathname:"/", state:{from: props.location}}}/>)
        }
    />
);