import React, {Component} from 'react';
import NavBar from "../atoms/NavBar";
import {Col, OverlayTrigger, Panel, Row, Tooltip} from "react-bootstrap";
import './MyPatients.scss';
import axios from "axios";

import PatientsTable from "../PatientsTable";

class MyPatients extends Component{

    constructor(props) {
        super(props);
        this.state={
            patients :''
        };
        this.options = {
            sortIndicator: true, // disable sort indicator
            defaultSortName: 'name',  // default sort column name*/
            defaultSortOrder: 'desc', // default sort order
            sizePerPageList: [
                {
                    text: '10',
                    value: 10
                },
                {
                    text: '25',
                    value: 25
                },
                {
                    text: '50',
                    value: 50
                },
                {
                    text: 'All',
                    value: 300
                }
            ], //pagination options
            sizePerPage: 50, // Which size per page to locate as default
            pageStartIndex: 1, // Where to start counting the pages
            paginationSize: 3, // The pagination bar size.
            prePage: '<', // Previous page button text
            nextPage: '>', // Next page button text
            firstPage: '<<', // First page button text
            lastPage: '>>', // Last page button text
            paginationShowsTotal: this.renderShowsTotal, //implemented
            paginationPanel: this.renderPaginationPanel, //implemented

        };
        this.renderPaginationPanel = this.renderPaginationPanel.bind(this);
        this.renderShowsTotal = this.renderShowsTotal.bind(this);
    }

    componentDidMount() {

        console.log("ALO GARA")
        if (localStorage.getItem('role') === 'CAREGIVER')
        {
            axios.post(`http://localhost:8080/caregiver/myPatients`, {id: localStorage.getItem('idCaregiver')} )
                .then(res => {
                    console.log(res.data);
                    this.setState({patients: res.data})
                });
        }

    }

    renderShowsTotal(start, to, total) {
        return <p style={{color: 'blue', marginTop: '10px'}}>Total: {total}&nbsp;&nbsp;</p>;
    }

    renderPaginationPanel(props) {
        return (
            <div className="pagination-style-patients">
                <div className="pagination-style-patients-dropDownMenu">
                    {props.components.totalText}
                    {props.components.sizePerPageDropdown}
                </div>
                <div className="pagination-list-patients">
                    {props.components.pageList}
                </div>
            </div>
        );
    }


    render() {
        console.log(this.state.patients);
        return (<div>
            <NavBar/>
            <Col xs={2}/>
            <Col xs={8}>
               <Panel className="patients-panel-container">
                    <Panel.Heading className="patients-panel-header">
                        <Row className="patients-panel-header-row">
                            <Col xs={11} className={'title-col-patients'}>
                                <p><b> Patients</b></p>
                            </Col>
                            <Col xs={1} className="info-col-patients">
                                <OverlayTrigger
                                    placement="bottom"
                                    overlay={<Tooltip id="info-table">Click on the row to view
                                        details</Tooltip>}
                                >
                                    <i className={'glyphicon glyphicon-info-sign'}/>
                                </OverlayTrigger>
                            </Col>
                        </Row>
                    </Panel.Heading>
                    <Panel.Body>
                        <PatientsTable
                            patients={this.state.patients}
                            options={this.options}
                        />
                    </Panel.Body>
                </Panel>
            </Col>
            <Col xs={2}/>
        </div>)
    }
}

export default MyPatients;