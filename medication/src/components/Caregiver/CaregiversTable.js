
import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import './CaregiversTable.scss'

function showName(cell){
    return cell.name;
}

function showDeleteIcon () {   // order is desc or asc
    return (<i className="glyphicon glyphicon-trash" /> )
}

const CaregiversTable = (props) => (<div id={'searchCaregivers'} className="table-style-caregivers">
    <BootstrapTable
        data={props.caregivers}
        pagination={true}
        options={props.options}
        /* selectRow={  {mode: 'checkbox',
             onSelect: props.onRowSelect,
             onSelectAll: props.onSelectAll }}*/
        striped={false}
        hover={true}
        bordered={false}
        condensed={true}
        trStyle={{cursor: 'pointer'}}
        headerStyle={{backgroundColor: 'transparent', borderRadius: '5px'}}>
        <TableHeaderColumn
            row="1"
            isKey
            dataField="id"
            hidden
        />
        <TableHeaderColumn
            row="1"
            width="150"
            dataField="name"

            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Name
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="birthDate"
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Birth Date
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="gender"
            dataSort
            headerAlign="left"
            dataAlign="left">
            Gender
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField='address'
            headerAlign="left"
            dataAlign="left">
            Address
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField='doctor'
            dataFormat={showName}
            headerAlign="left"
            dataAlign="left">
            Doctor
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="50"
            dataAlign="center"
            dataFormat={()=> {return (<i className="glyphicon glyphicon-trash" onClick={props.showDeletePopUp}/> )}}
        />
    </BootstrapTable>
</div>);

export default CaregiversTable;