import React, {Component} from 'react';
import NavBar from "./atoms/NavBar";
import {Button, ButtonGroup, ButtonToolbar, Col, OverlayTrigger, Panel, Row, Tooltip} from "react-bootstrap";
import './MyCaregivers.scss';
import axios from "axios";
import CaregiversTable from "./Caregiver/CaregiversTable";
import CaregiverDetails from "./Caregiver/CaregiverDetails";
import DeletePopUp from "./atoms/DeletePopUp"


class MyCaregivers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            caregivers: '',
            modalForAdd: false,
            showModal: false,
            selectedRow: '',
            showDeletePopUp: false,
            doctors:''
        };
        this.options = {
            sortIndicator: true, // disable sort indicator
            defaultSortName: 'name',  // default sort column name*/
            defaultSortOrder: 'desc', // default sort order
            sizePerPageList: [
                {
                    text: '10',
                    value: 10
                },
                {
                    text: '25',
                    value: 25
                },
                {
                    text: '50',
                    value: 50
                },
                {
                    text: 'All',
                    value: 300
                }
            ], //pagination options
            sizePerPage: 50, // Which size per page to locate as default
            pageStartIndex: 1, // Where to start counting the pages
            paginationSize: 3, // The pagination bar size.
            prePage: '<', // Previous page button text
            nextPage: '>', // Next page button text
            firstPage: '<<', // First page button text
            lastPage: '>>', // Last page button text
            paginationShowsTotal: this.renderShowsTotal, //implemented
            paginationPanel: this.renderPaginationPanel, //implemented
            onRowClick: this.showModalForRow
        };

        this.renderShowsTotal = this.renderShowsTotal.bind(this);
        this.showModalForAdd = this.showModalForAdd.bind(this);
        //this.showModalForRow = this.showModalForRow.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.clearErrorMessage = this.clearErrorMessage.bind(this);
        this.validationBeforeSubmit = this.validationBeforeSubmit.bind(this);
        // this.submitRow = this.submitRow.bind(this);


    }

    componentDidMount() {
        let caregiverDTO = {
            name: 'Mimi',
            birthDate: '2018-09-09',
            gender: 'F',
            address: 'adr1'
        };
        let userDTO = {
            username: 'caregiver45',
            password: 'caregiver45'
        };

       // axios.post(`http://localhost:8080/doctor/addCaregiver`,  {caregiverDTO, userDTO}, {params: {idDoctor: localStorage.getItem('idDoctor')}}  );
        /* merge!!!

axios.post(`http://localhost:8080/doctor/myCaregivers`, {id: localStorage.getItem('idDoctor')})
                .then(res => {
                    console.log(res.data);
                    this.setState({caregivers: res.data})
                });
         */
        if (localStorage.getItem('role') === 'DOCTOR') {
            axios.get(`http://localhost:8080/doctor/allCaregivers`)
                .then(res => {
                    console.log(res.data);
                    this.setState({caregivers: res.data})
                });
            axios.get(`http://localhost:8080/doctor/allDoctors`).then(res=>{
                console.log(res.data);
                this.setState({doctors: res.data})
            })
        }

    }

    /*  componentDidUpdate(prevProps, prevState, snapshot) {
          if(prevState.showModal!==this.state.showModal){
              if (localStorage.getItem('role') === 'DOCTOR')
              {
                  axios.post(`http://localhost:8080/doctor/myCaregivers`, {id: localStorage.getItem('idDoctor')} )
                      .then(res => {
                          console.log(res.data);
                          this.setState({caregivers: res.data})
                      });
              }
          }
      }*/

    showDeletePopUp =()  =>{
       this.setState({showDeletePopUp: true});
    };

    deleteRow = () =>{
        axios.delete(`http://localhost:8080/doctor/deleteCaregiver`,{params:{id: this.state.selectedRow.id}})
            .then(()=>{
                axios.get(`http://localhost:8080/doctor/allCaregivers`)
                    .then(res => {
                        console.log(res.data);
                        this.setState({caregivers: res.data})
                    });
            });
        this.hideDeletePopUp();
    };

    hideDeletePopUp = () =>{
        this.setState({showDeletePopUp: false});
    };

    showModalForRow = (row, columnIndex, rowIndex) => {
        console.log(columnIndex);
        this.setState({
            selectedRow: row,})
        if (columnIndex !==5 && columnIndex!== undefined){
            this.setState({showModal:true})
        };
    };

    showModalForAdd() {
                    this.setState({
                        showModal: true,
                        modalForAdd: true,
                        selectedRow: {
                            username: '', password: '', name: '', birthDate: '', gender: 'M', address: '', idDoctor:'1'
            }
        });
    }

    clearErrorMessage() {
        this.setState({errorMessage: ''});
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        let row1;
        row1 = {...this.state.selectedRow};
        row1[name] = value;
        this.setState({selectedRow: row1});
    };

    handleDate = (value) => {
        let row1, data;
        if (value) data = value.split("T")[0];
        row1 = {...this.state.selectedRow};
        row1['birthDate'] = data;
        this.setState({selectedRow: row1});
    }


    hideModal() {
        this.setState({showModal: false, modalForAdd: false});
        this.clearErrorMessage();
    }

    validationBeforeSubmit() {
        return true;
    }

    submitRow = (e) => {


        let userDTO = {
            username: this.state.selectedRow.username,
            password: this.state.selectedRow.password
        };



        e.preventDefault();
        if (this.validationBeforeSubmit()) {
            this.clearErrorMessage();
            if (this.state.modalForAdd) {   //if a row is added
                let caregiverDTO= {
                    name: this.state.selectedRow.name,
                    birthDate: this.state.selectedRow.birthDate,
                    gender: this.state.selectedRow.gender,
                    address: this.state.selectedRow.address
                };
                axios.post(`http://localhost:8080/doctor/addCaregiver`, {
                    caregiverDTO,
                    userDTO
                }, {params: {idDoctor :this.state.selectedRow.idDoctor}}).then(
                    () => {
                        axios.get(`http://localhost:8080/doctor/allCaregivers`)
                            .then(res => {
                                console.log(res.data);
                                this.setState({caregivers: res.data})
                            });
                    }
                )
                this.hideModal();
            } else {
                let caregiverDTO = {
                    id: this.state.selectedRow.id,
                    name: this.state.selectedRow.name,
                    birthDate: this.state.selectedRow.birthDate,
                    gender: this.state.selectedRow.gender,
                    address: this.state.selectedRow.address
                }// if a row is updated
                axios.post(`http://localhost:8080/doctor/updateCaregiver`, caregiverDTO, {params: {idDoctor :this.state.selectedRow.idDoctor}})
                    .then(() => {
                        axios.get(`http://localhost:8080/doctor/allCaregivers`)
                            .then(res => {
                                console.log(res.data);
                                this.setState({caregivers: res.data})
                            });
                    });
                this.hideModal();
            }
        }
    }


    renderShowsTotal = (start, to, total) => {
        return <p style={{color: 'blue', marginTop: '10px'}}>Total: {total}&nbsp;&nbsp;</p>;
    }

    renderPaginationPanel = (props) => {
        return (
            <div className="pagination-style-caregivers">
                <div className="pagination-style-caregivers-dropDownMenu">
                    {props.components.totalText}
                    {props.components.sizePerPageDropdown}
                </div>
                <div className="pagination-list-caregivers">
                    <ButtonToolbar className="table-buttons-container-caregivers">
                        <ButtonGroup bsClass="add-delete-print-buttons-caregivers">
                            <OverlayTrigger
                                overlay={<Tooltip id="info-table">Add Caregiver</Tooltip>}
                                placement="bottom"
                            >
                                <Button bsStyle="success" className="button-add-caregivers"
                                        onClick={this.showModalForAdd}>
                                    <i className="glyphicon glyphicon-plus"/>
                                </Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </ButtonToolbar>
                    {props.components.pageList}
                </div>
            </div>
        );
    }


    render() {
        console.log(this.state.selectedRow);
        return (<div>
            <NavBar/>
            <Col xs={2}/>
            <Col xs={8}>
                <Panel className="caregivers-panel-container">
                    <Panel.Heading className="caregivers-panel-header">
                        <Row className="caregivers-panel-header-row">
                            <Col xs={11} className={'title-col-caregivers'}>
                                <p><b> Caregivers</b></p>
                            </Col>
                            <Col xs={1} className="info-col-caregivers">
                                <OverlayTrigger
                                    placement="bottom"
                                    overlay={<Tooltip id="info-table">Click on the row to view
                                        details</Tooltip>}
                                >
                                    <i className={'glyphicon glyphicon-info-sign'}/>
                                </OverlayTrigger>
                            </Col>
                        </Row>
                    </Panel.Heading>
                    <Panel.Body>
                        <CaregiversTable
                            caregivers={this.state.caregivers}
                            options={this.options}
                            showDeletePopUp={this.showDeletePopUp}

                        />
                    </Panel.Body>
                </Panel>
            </Col>
            <Col xs={2}/>
            <div>
                <CaregiverDetails
                    showModal={this.state.showModal}
                    showModalForAdd={this.state.modalForAdd}
                    hideModal={this.hideModal}
                    handleChange={this.handleChange}
                    handleDate={this.handleDate}
                    selectedRow={this.state.selectedRow}
                    submitRow={this.submitRow}
                    doctors={this.state.doctors}
                />
            </div>
            <Col>
                <DeletePopUp
                    showDeletePopUp={this.state.showDeletePopUp}
                    hideDeletePopUp={this.hideDeletePopUp}
                    deleteRow={this.deleteRow}
                />
            </Col>
        </div>)
    }
}

export default MyCaregivers;