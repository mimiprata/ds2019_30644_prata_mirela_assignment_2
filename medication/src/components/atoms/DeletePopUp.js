import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import './DeletePopUp.scss';

const DeletePopUp = (props) =>
    (<Modal
        show={props.showDeletePopUp}
        onHide={props.hideDeletePopUp}
        dialogClassName="delete-modal-caregivers"
    >
        <Modal.Body>

            <p className="text-delete-pop-up-caregivers">
                    Are you sure you want to delete this?
            </p>

        </Modal.Body>

        <div className="buttons-delete-pop-up-caregivers">
            <Button
                onClick={props.deleteRow}
                className="button-ok-caregivers"
                bsStyle={null}
            >
                <i className="glyphicon glyphicon-ok"/>
            </Button>
            <Button
                onClick={props.hideDeletePopUp}
                className="button-remove-caregivers"
                bsStyle={null}
            >
                <i className="glyphicon glyphicon-remove"/>
            </Button>
        </div>
    </Modal>);

export default DeletePopUp;