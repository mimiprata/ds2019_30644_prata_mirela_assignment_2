import React from 'react';
import {Form, Row, Col, ControlLabel, FormControl, Button} from 'react-bootstrap';

const LoginForm = (props) => {
    return (
        <Form>
            <Row>
            <Col xs={4}><ControlLabel>Username</ControlLabel></Col>

            <Col xs={8}> <FormControl
                type="text"
                name={"username"}
                value={props.username}
                placeholder="Enter username"
                onChange={(e)=>{props.handleChange(e)}}/></Col>
            </Row>
            <Row>
            <Col xs={4}><ControlLabel>Password</ControlLabel></Col>
            <Col xs={8}> <FormControl
                type="password"
                name={"password"}
                value={props.password}
                placeholder="Enter password"
                onChange={(e)=>{props.handleChange(e)}}/></Col>
            </Row>
            <Row>
                <Col xs={12}>
                <Button bsStyle="primary" onClick={props.handleSubmit}>
                    LOGIN
                </Button>
                </Col>
            </Row>
        </Form>
    );


}

export default LoginForm;