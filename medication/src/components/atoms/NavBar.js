import React from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import { Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";


const NavBar = (props) => {
    return(<Navbar  collapseOnSelect>
        <Navbar.Header>
            <Navbar.Brand>
                <Link to="/home">Home</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                {
                    localStorage.getItem('role') ==='PATIENT' ||  localStorage.getItem('role') ==='CAREGIVER' ?
                    <LinkContainer to="/myAccount">
                        <NavItem eventKey={1}>
                            My Profile
                        </NavItem>
                    </LinkContainer>:
                        null
                }
                {
                    localStorage.getItem('role') ==='PATIENT' ?
                        <LinkContainer to="/myMedicationPlan">
                         <NavItem eventKey={4}>
                            My Medication Plan
                         </NavItem>
                        </LinkContainer>:
                        null
                }
                {
                    localStorage.getItem('role') ==='CAREGIVER'  ?
                        <LinkContainer to="/myPatients">
                    <NavItem eventKey={2}>
                        My Patients
                    </NavItem>
                        </LinkContainer>
                        :
                        null
                }

                {
                    localStorage.getItem('role') ==='DOCTOR'  ?
                        <LinkContainer to="/patients">
                            <NavItem eventKey={2}>
                                Patients
                            </NavItem>
                        </LinkContainer>
                        :
                        null
                }

                {
                     localStorage.getItem('role') === 'DOCTOR' ?
                        <NavItem eventKey={3} href="/myCaregivers">
                            Caregivers
                        </NavItem>
                        :
                        null
                }

                {
                    localStorage.getItem('role') ==='DOCTOR'  ?
                        <LinkContainer to="/medication">
                            <NavItem eventKey={4}>
                               Medication
                            </NavItem>
                        </LinkContainer>
                        :
                        null
                }
            </Nav>
            <Nav pullRight>
                <LinkContainer to="/logout">
                <NavItem eventKey={1} >
                    Logout
                </NavItem>
                </LinkContainer>
            </Nav>
        </Navbar.Collapse>
    </Navbar>)

};

export default NavBar;