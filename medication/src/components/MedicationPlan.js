import React, {Component} from 'react';
import {Col, ControlLabel} from "react-bootstrap";
import axios from "axios";
import NavBar from "./atoms/NavBar";
import './MedicationPlan.scss'

class MedicationPlan extends Component{


    constructor(props) {
        super(props);
        this.state={
            intakes:''
        }

    }

    componentWillMount() {
        if (localStorage.getItem('role')=== 'PATIENT'){
            axios.post(`http://localhost:8080/getIntakes`, {id: localStorage.getItem('idPatient')} )
                .then(res => {
                    console.log(res.data);
                    this.setState({intakes: res.data})
                });
        }
    }

    render() {
        console.log(this.state.intakes);
        return(
            <div>
                <NavBar/>
                <Col xsHidden sm={2}/>
                <Col xs={12} sm={4}>


                            <div className="list-content-medication">
                                {this.state.intakes &&
                                this.state.intakes.map((item) => {
                                    return (<div className="item-list-content-medication">
                                        <h4>
                                        {item.medication.name.toUpperCase()}
                                    </h4>

                                        {item.medication.description}
                                        <br/>
                                        <br/>
                                        <b>
                                            Please take this according to:
                                        </b>
                                        <br/>
                                        {item.details} <br/>
                                        <br/>
                                        <b>
                                            Start Date
                                        </b>
                                        <br/>
                                        {item.startDate}
                                        <br/>
                                        <br/>
                                        <b>
                                            End Date
                                        </b>
                                        <br/>
                                        {item.endDate}


                                    </div>);
                                })
                                }
                            </div>

                </Col>
                <Col xsHidden sm={6}/>
            </div>
        )
    }


};

export default MedicationPlan;