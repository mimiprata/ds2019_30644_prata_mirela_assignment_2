
import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Button} from 'react-bootstrap';
import './IntakeTable.scss'

function showNameM(cell){
    return cell.name;
}

const IntakeTable = (props) => (<div id={'searchIntakes'} className="table-style-intake">
    <BootstrapTable


        data={props.intakes}
        pagination={true}
        options={props.options}
        /* selectRow={  {mode: 'checkbox',
             onSelect: props.onRowSelect,
             onSelectAll: props.onSelectAll }}*/
        striped={false}
        hover={true}
        bordered={false}
        condensed={true}
        trStyle={{cursor: 'pointer'}}
        headerStyle={{backgroundColor: 'transparent', borderRadius: '5px'}}>
        <TableHeaderColumn
            dataField="id"
            isKey
            hidden
        />
        <TableHeaderColumn
            row="1"
            width="150"
            dataField="medication"
            dataFormat={showNameM}
            headerAlign="left"
            dataAlign="left">
            Medication
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="150"
            dataField="startDate"
            headerAlign="left"
            dataAlign="left">
            Start Date
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="endDate"
            headerAlign="left"
            dataAlign="left">
            End Date
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="details"
            dataSort
            headerAlign="left"
            dataAlign="left">
            Details
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="50"
            dataAlign="center"
            dataFormat={()=> {return (<i className="glyphicon glyphicon-trash" onClick={props.showDeletePopUp}/> )}}
        />

    </BootstrapTable>
</div>);

export default IntakeTable;