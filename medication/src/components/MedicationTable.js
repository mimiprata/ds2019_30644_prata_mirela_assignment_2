
import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import './MedicationTable.scss'



const  MedicationTable = (props) => (<div id={'searchMedication'} className="table-style-medication">
    <BootstrapTable
        data={props.medications}
        pagination={true}
        options={props.options}
        /* selectRow={  {mode: 'checkbox',
             onSelect: props.onRowSelect,
             onSelectAll: props.onSelectAll }}*/
        striped={false}
        hover={true}
        bordered={false}
        condensed={true}
        trStyle={{cursor: 'pointer'}}
        headerStyle={{backgroundColor: 'transparent', borderRadius: '5px'}}>
        <TableHeaderColumn
            row="1"
            width="150"
            dataField="name"
            isKey
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Name
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="description"
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Description
        </TableHeaderColumn>
        
                <TableHeaderColumn
                    row="1"
                    width="50"
                    dataAlign="center"
                    dataFormat={()=> {return (<i className="glyphicon glyphicon-trash" onClick={props.showDeletePopUp}/> )}}
                />
    </BootstrapTable>
</div>);

export default MedicationTable;