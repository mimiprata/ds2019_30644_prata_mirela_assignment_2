
import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import './PatientsTable.scss'

function showName(cell){
    return cell.name;
}

const PatientsAsDoctorTable = (props) => (<div id={'searchPatients'} className="table-style-patients">
    <BootstrapTable
        data={props.patients}
        pagination={true}
        options={props.options}
        /* selectRow={  {mode: 'checkbox',
             onSelect: props.onRowSelect,
             onSelectAll: props.onSelectAll }}*/
        striped={false}
        hover={true}
        bordered={false}
        condensed={true}
        trStyle={{cursor: 'pointer'}}
        headerStyle={{backgroundColor: 'transparent', borderRadius: '5px'}}>
        <TableHeaderColumn
            row="1"
            width="150"
            dataField="name"
            isKey
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Name
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="birthDate"
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Birth Date
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="gender"
            dataSort
            headerAlign="left"
            dataAlign="left">
            Gender
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField='address'
            dataSort={true}
            sortOrder="asc"
            headerAlign="left"
            dataAlign="left">
            Address
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="medicalRecord"
            dataSort = {true}
            headerAlign="left"
            dataAlign="left">
            Medical Record
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="caregiver"
            dataFormat={showName}
            dataSort = {true}
            headerAlign="left"
            dataAlign="left">
            Medical Record
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="50"
            dataAlign="center"
            dataFormat={()=> {return (<i className="glyphicon glyphicon-trash" /> )}}
        />

    </BootstrapTable>
</div>);

export default PatientsAsDoctorTable;