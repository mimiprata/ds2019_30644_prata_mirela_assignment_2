import React, {Component} from 'react';
import axios from "axios";
import {Button, ButtonGroup, ButtonToolbar, Col, OverlayTrigger, Panel, Row, Tooltip} from "react-bootstrap";
import NavBar from "./atoms/NavBar";
import DeletePopUp from "./atoms/DeletePopUp";
import MedicationTable from "./MedicationTable";
import MedicationDetails from "./MedicationDetails";

class Medication extends Component {

    constructor(props) {
        super(props);
        this.state = {
            medications: '',
            modalForAdd: false,
            showModal: false,
            selectedRow: '',
            showDeletePopUp: false
        };
        this.options = {
            sortIndicator: true, // disable sort indicator
            defaultSortName: 'name',  // default sort column name*/
            defaultSortOrder: 'desc', // default sort order
            sizePerPageList: [
                {
                    text: '10',
                    value: 10
                },
                {
                    text: '25',
                    value: 25
                },
                {
                    text: '50',
                    value: 50
                },
                {
                    text: 'All',
                    value: 300
                }
            ], //pagination options
            sizePerPage: 50, // Which size per page to locate as default
            pageStartIndex: 1, // Where to start counting the pages
            paginationSize: 3, // The pagination bar size.
            prePage: '<', // Previous page button text
            nextPage: '>', // Next page button text
            firstPage: '<<', // First page button text
            lastPage: '>>', // Last page button text
            paginationShowsTotal: this.renderShowsTotal, //implemented
            paginationPanel: this.renderPaginationPanel, //implemented
            onRowClick: this.showModalForRow
        };
        this.renderPaginationPanel = this.renderPaginationPanel.bind(this);
        this.renderShowsTotal = this.renderShowsTotal.bind(this);
    }

    componentDidMount() {


          if (localStorage.getItem('role') === 'DOCTOR')
          {
              //axios.post(`http://localhost:8080/doctor/updatePatient`,  patientDTO, {params: {idCaregiver:2}}  );
              axios.get(`http://localhost:8080/doctor/allMedications`,  )
                  .then(res => {
                      console.log(res.data);
                      this.setState({medications: res.data})
                  });
          }

    }

    showDeletePopUp = () => {
        this.setState({showDeletePopUp: true});
    };

    deleteRow = () => {
        axios.delete(`http://localhost:8080/doctor/deleteMedication`,{params:{id: this.state.selectedRow.id}})
            .then(()=>{
                axios.get(`http://localhost:8080/doctor/allMedications`,)
                    .then(res => {
                        console.log(res.data);
                        this.setState({medications: res.data})
                    });
            });
        this.hideDeletePopUp();
    };

    hideDeletePopUp = () => {
        this.setState({showDeletePopUp: false});
    };

    showModalForRow = (row, columnIndex, rowIndex) => {
        console.log(columnIndex);
        this.setState({
            selectedRow: row,
        })
        if (columnIndex !== 6 && columnIndex !== undefined) {
            this.setState({showModal: true})
        }
        ;
    };

    showModalForAdd = () => {
        this.setState({
            showModal: true,
            modalForAdd: true,
            selectedRow: {
                name: '', description: ''
            }
        });
    };

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        let row1;
        row1 = {...this.state.selectedRow};
        row1[name] = value;
        this.setState({selectedRow: row1});
    };


    hideModal = () => {
        this.setState({showModal: false, modalForAdd: false});
        // this.clearErrorMessage();
    };

    validationBeforeSubmit = () => {
        return true;
    };

    submitRow = (e) => {


        e.preventDefault();
        if (this.validationBeforeSubmit()) {
            //this.clearErrorMessage();
            if (this.state.modalForAdd) {   //if a row is added
                let medicationDTO = {
                    name: this.state.selectedRow.name,
                    description: this.state.selectedRow.description
                };
                 axios.post(`http://localhost:8080/doctor/addMedication`, medicationDTO).then(
                     () => {
                         axios.get(`http://localhost:8080/doctor/allMedications`)
                             .then(res => {
                                 console.log(res.data);
                                 this.setState({medications: res.data})
                             });
                     }
                 )
                this.hideModal();
            } else {
                let medicationDTO = {
                    id:this.state.selectedRow.id,
                    name: this.state.selectedRow.name,
                    description: this.state.selectedRow.description
                };
                axios.post(`http://localhost:8080/doctor/updateMedication`, medicationDTO).then(
                    () => {
                        axios.get(`http://localhost:8080/doctor/allMedications`)
                            .then(res => {
                                console.log(res.data);
                                this.setState({medications: res.data})
                            });
                    }
                )

                this.hideModal();
            }
        }
    }


    renderShowsTotal(start, to, total) {
        return <p style={{color: 'blue', marginTop: '10px'}}>Total: {total}&nbsp;&nbsp;</p>;
    }

    renderPaginationPanel = (props) => {
        return (
            <div className="pagination-style-patients">
                <div className="pagination-style-patients-dropDownMenu">
                    {props.components.totalText}
                    {props.components.sizePerPageDropdown}
                </div>
                <div className="pagination-list-patients">
                    <ButtonToolbar className="table-buttons-container-caregivers">
                        <ButtonGroup bsClass="add-delete-print-buttons-caregivers">
                            <OverlayTrigger
                                overlay={<Tooltip id="info-table">Add Medication</Tooltip>}
                                placement="bottom"
                            >
                                <Button bsStyle="success" className="button-add-caregivers"
                                        onClick={this.showModalForAdd}>
                                    <i className="glyphicon glyphicon-plus"/>
                                </Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </ButtonToolbar>
                    {props.components.pageList}
                </div>
            </div>
        );
    }

    render() {
        console.log(this.state.selectedRow);
        return (<div>
            <NavBar/>
            <Col xs={2}/>
            <Col xs={8}>
                <Panel className="patients-panel-container">
                    <Panel.Heading className="patients-panel-header">
                        <Row className="patients-panel-header-row">
                            <Col xs={11} className={'title-col-patients'}>
                                <p><b> Medications </b></p>
                            </Col>
                            <Col xs={1} className="info-col-patients">
                                <OverlayTrigger
                                    placement="bottom"
                                    overlay={<Tooltip id="info-table">Click on the row to view
                                        details</Tooltip>}
                                >
                                    <i className={'glyphicon glyphicon-info-sign'}/>
                                </OverlayTrigger>
                            </Col>
                        </Row>
                    </Panel.Heading>
                    <Panel.Body>
                        <MedicationTable
                            medications={this.state.medications}
                            options={this.options}
                            showDeletePopUp={this.showDeletePopUp}
                        />
                    </Panel.Body>
                </Panel>
            </Col>
            <Col xs={2}/>
            <div>
                <MedicationDetails
                    handleChange={this.handleChange}
                    submitRow={this.submitRow}
                    showModal={this.state.showModal}
                    showModalForAdd={this.state.modalForAdd}
                    hideModal={this.hideModal}
                    selectedRow={this.state.selectedRow}
                />
            </div>
            <Col>
                <DeletePopUp
                    showDeletePopUp={this.state.showDeletePopUp}
                    hideDeletePopUp={this.hideDeletePopUp}
                    deleteRow={this.deleteRow}
                />
            </Col>
        </div>)
    }

};
export default Medication;