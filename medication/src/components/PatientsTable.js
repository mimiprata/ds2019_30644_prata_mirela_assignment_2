
import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import {Button} from 'react-bootstrap';
import './PatientsTable.scss'
import {Link} from "react-router-dom";

function showName(cell){
    return cell.name;
}

const PatientsTable = (props) => (<div id={'searchPatients'} className="table-style-patients">
    <BootstrapTable
        data={props.patients}
        pagination={true}
        options={props.options}
       /* selectRow={  {mode: 'checkbox',
            onSelect: props.onRowSelect,
            onSelectAll: props.onSelectAll }}*/
        striped={false}
        hover={true}
        bordered={false}
        condensed={true}
        trStyle={{cursor: 'pointer'}}
        headerStyle={{backgroundColor: 'transparent', borderRadius: '5px'}}>


        <TableHeaderColumn
            row="1"
            width="150"
            dataField="name"
            isKey
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Name
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="birthDate"
            dataSort={true}
            headerAlign="left"
            dataAlign="left">
            Birth Date
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="gender"
            dataSort
            headerAlign="left"
            dataAlign="left">
            Gender
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField='address'
            dataSort={true}
            sortOrder="asc"
            headerAlign="left"
            dataAlign="left">
            Address
        </TableHeaderColumn>
        <TableHeaderColumn
            row="1"
            width="100"
            dataField="medicalRecord"
            dataSort = {true}
            headerAlign="left"
            dataAlign="left">
            Medical Record
        </TableHeaderColumn>

        {
            props.doctor===true ?
                <TableHeaderColumn
                    row="1"
                    width="100"
                    dataField="caregiver"
                    dataSort = {true}
                    dataFormat={showName}
                    headerAlign="left"
                    dataAlign="left">
                    Caregiver
                </TableHeaderColumn>:
                null
        }

        {
            props.doctor===true ?
                <TableHeaderColumn
                    row="1"
                    width="100"
                    dataAlign="center"
                    dataFormat={()=> {return (<i className="glyphicon glyphicon-list" onClick={props.openMedicationPlan}/>)}}
                >Medication Plan</TableHeaderColumn>:
                null
        }


        {
            props.doctor===true ?
                <TableHeaderColumn
                    row="1"
                    width="50"
                    dataAlign="center"
                    dataFormat={()=> {return (<i className="glyphicon glyphicon-trash" onClick={props.showDeletePopUp}/> )}}
                />:
                null
        }
    </BootstrapTable>
</div>);

export default PatientsTable;