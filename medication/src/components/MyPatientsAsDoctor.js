import React , {Component} from 'react';
import axios from "axios";
import NavBar from "./atoms/NavBar";
import {Button, ButtonGroup, ButtonToolbar, Col, OverlayTrigger, Panel, Row, Tooltip} from "react-bootstrap";
import PatientsTable from "./PatientsTable";
import PatientDetails from "./PatientDetails";
import DeletePopUp from "./atoms/DeletePopUp";
import {Redirect} from "react-router-dom";
import EditMedicationPlan from "./EditMedicationPlan";

class MyPatientsAsDoctor extends Component {

    constructor(props) {
        super(props);
        this.state={
            patients :'',
            modalForAdd: false,
            showModal: false,
            selectedRow: '',
            showDeletePopUp: false,
            allCaregivers:'',
            redirect: false,
            intakes : '',
            showMedicationPlan:false
        };
        this.options = {
            sortIndicator: true, // disable sort indicator
            defaultSortName: 'name',  // default sort column name*/
            defaultSortOrder: 'desc', // default sort order
            sizePerPageList: [
                {
                    text: '10',
                    value: 10
                },
                {
                    text: '25',
                    value: 25
                },
                {
                    text: '50',
                    value: 50
                },
                {
                    text: 'All',
                    value: 300
                }
            ], //pagination options
            sizePerPage: 50, // Which size per page to locate as default
            pageStartIndex: 1, // Where to start counting the pages
            paginationSize: 3, // The pagination bar size.
            prePage: '<', // Previous page button text
            nextPage: '>', // Next page button text
            firstPage: '<<', // First page button text
            lastPage: '>>', // Last page button text
            paginationShowsTotal: this.renderShowsTotal, //implemented
            paginationPanel: this.renderPaginationPanel, //implemented
            onRowClick: this.showModalForRow
        };
        this.renderPaginationPanel = this.renderPaginationPanel.bind(this);
        this.renderShowsTotal = this.renderShowsTotal.bind(this);
    }

    componentDidMount() {

        let patientDTO = {
            id:"7",
            name: 'Mimi',
            birthDate: '2018-09-09',
            gender: 'F',
            address: 'adr1',
            medicalRecord:'none'
        };
        let userDTO = {
            username: 'p1',
            password: 'p1'
        };


        if (localStorage.getItem('role') === 'DOCTOR')
        {
            //axios.post(`http://localhost:8080/doctor/updatePatient`,  patientDTO, {params: {idCaregiver:2}}  );
            axios.get(`http://localhost:8080/doctor/myPatients`,  )
                .then(res => {
                    console.log(res.data);
                    this.setState({patients: res.data})
                });
            axios.get(`http://localhost:8080/doctor/allCaregivers`).then(res=>{
                console.log(res.data);
                this.setState({allCaregivers: res.data})
            })
        }

    }

    showDeletePopUp =()  =>{
        this.setState({showDeletePopUp: true});
    };

    deleteRow = () =>{
        axios.delete(`http://localhost:8080/doctor/deletePatient`,{params:{id: this.state.selectedRow.id}})
            .then(()=>{
                axios.get(`http://localhost:8080/doctor/myPatients`,)
                    .then(res => {
                        console.log(res.data);
                        this.setState({patients: res.data})
                    });
            });
        this.hideDeletePopUp();
    };

    hideDeletePopUp = () =>{
        this.setState({showDeletePopUp: false});
    };

    hideMedicationPlan= () =>{
        this.setState({showMedicationPlan:false})
    }

    showModalForRow = (row, columnIndex, rowIndex) => {
        console.log(columnIndex);
        this.setState({
            selectedRow: row,})
        if ( columnIndex!== 7 && columnIndex!==6 && columnIndex!== undefined){
            this.setState({showModal:true})
        };

        if( columnIndex===6){
            this.setState({showMedicationPlan:true});
            axios.post(`http://localhost:8080/getIntakes`, {id:this.state.selectedRow.id})
                .then(res => {
                    console.log(res.data);
                    this.setState({intakes: res.data})
                });
        }

    };

    showModalForAdd = () =>{
        this.setState({
            showModal: true,
            modalForAdd: true,
            selectedRow: {
                username: '', password: '', name: '', birthDate: '', gender: 'M', address: '', medicalRecord:'', idCaregiver:'1'
            }
        });
    };

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        let row1;
        row1 = {...this.state.selectedRow};
        row1[name] = value;
        this.setState({selectedRow: row1});
    };

    handleDate = (value) => {
        let row1, data;
        if (value) data = value.split("T")[0];
        row1 = {...this.state.selectedRow};
        row1['birthDate'] = data;
        this.setState({selectedRow: row1});
    };


    hideModal =() =>{
        this.setState({showModal: false, modalForAdd: false});
       // this.clearErrorMessage();
    };

    validationBeforeSubmit = () =>{
        return true;
    };

    submitRow = (e) => {

        let userDTO = {
            username: this.state.selectedRow.username,
            password: this.state.selectedRow.password
        };

        /*let caregiverDTO = {
            id: this.state.selectedRow.id,
            name: this.state.selectedRow.name,
            birthDate: this.state.selectedRow.birthDate,
            gender: this.state.selectedRow.gender,
            address: this.state.selectedRow.address
        }*/
     /*   console.log(JSON.stringify({
            patientDTONew,
            userDTO
        }));*/

        e.preventDefault();
        if (this.validationBeforeSubmit()) {
            //this.clearErrorMessage();
            if (this.state.modalForAdd) {   //if a row is added
                let patientDTO = {
                    name: this.state.selectedRow.name,
                    birthDate: this.state.selectedRow.birthDate,
                    gender: this.state.selectedRow.gender,
                    address: this.state.selectedRow.address,
                    medicalRecord: this.state.selectedRow.medicalRecord
                };
                axios.post(`http://localhost:8080/doctor/addPatient`, {
                    patientDTO,
                    userDTO
                }, {params: {idCaregiver: this.state.selectedRow.idCaregiver}}).then(
                    () => {
                        axios.get(`http://localhost:8080/doctor/myPatients`)
                            .then(res => {
                                console.log(res.data);
                                this.setState({patients: res.data})
                            });
                    }
                )
                this.hideModal();
            } else {
                let patientDTO = {
                    id: this.state.selectedRow.id,
                    name: this.state.selectedRow.name,
                    birthDate: this.state.selectedRow.birthDate,
                    gender: this.state.selectedRow.gender,
                    address: this.state.selectedRow.address,
                    medicalRecord: this.state.selectedRow.medicalRecord
                };// if a row is updated
                axios.post(`http://localhost:8080/doctor/updatePatient`,
                    patientDTO, {params: {idCaregiver: this.state.selectedRow.caregiver.id}}).then(
                    () => {
                        axios.get(`http://localhost:8080/doctor/myPatients`)
                            .then(res => {
                                console.log(res.data);
                                this.setState({patients: res.data})
                            });
                    }
                )
                this.hideModal();
            }
        }
    }

    openMedicationPlan = () =>{

        localStorage.setItem('idP',this.state.selectedRow.id);
    }




    renderShowsTotal(start, to, total) {
        return <p style={{color: 'blue', marginTop: '10px'}}>Total: {total}&nbsp;&nbsp;</p>;
    }

    renderPaginationPanel = (props) =>{
        return (
            <div className="pagination-style-patients">
                <div className="pagination-style-patients-dropDownMenu">
                    {props.components.totalText}
                    {props.components.sizePerPageDropdown}
                </div>
                <div className="pagination-list-patients">
                    <ButtonToolbar className="table-buttons-container-caregivers">
                        <ButtonGroup bsClass="add-delete-print-buttons-caregivers">
                            <OverlayTrigger
                                overlay={<Tooltip id="info-table">Add Patient</Tooltip>}
                                placement="bottom"
                            >
                                <Button bsStyle="success" className="button-add-caregivers"
                                        onClick={this.showModalForAdd}>
                                    <i className="glyphicon glyphicon-plus"/>
                                </Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </ButtonToolbar>
                    {props.components.pageList}
                </div>
            </div>
        );
    }

    render(){
        console.log(this.state.selectedRow);
        /*
        console.log(this.state.showDeletePopUp);
        console.log(this.state.intakes);
        if (this.state.redirect ===true) {
            console.log(this.state.selectedRow);
            return (<Redirect to={{
                pathname: "/editMedicationPlan",
                state: {row: this.state.selectedRow}
            }}/>)
        }*/
        return (


            <div>
            <NavBar/>
            <Col xs={2}/>
            <Col xs={8}>
                <Panel className="patients-panel-container">
                    <Panel.Heading className="patients-panel-header">
                        <Row className="patients-panel-header-row">
                            <Col xs={11} className={'title-col-patients'}>
                                <p><b> Patients</b></p>
                            </Col>
                            <Col xs={1} className="info-col-patients">
                                <OverlayTrigger
                                    placement="bottom"
                                    overlay={<Tooltip id="info-table">Click on the row to view
                                        details</Tooltip>}
                                >
                                    <i className={'glyphicon glyphicon-info-sign'}/>
                                </OverlayTrigger>
                            </Col>
                        </Row>
                    </Panel.Heading>
                    <Panel.Body>
                        <PatientsTable
                            patients={this.state.patients}
                            options={this.options}
                            doctor={true}
                            showDeletePopUp={this.showDeletePopUp}
                            openMedicationPlan={this.openMedicationPlan}
                        />
                    </Panel.Body>
                </Panel>
            </Col>
            <Col xs={2}/>
            <div>
                <PatientDetails
                    showModal={this.state.showModal}
                    showModalForAdd={this.state.modalForAdd}
                    hideModal={this.hideModal}
                    handleChange={this.handleChange}
                    handleDate={this.handleDate}
                    selectedRow={this.state.selectedRow}
                    submitRow={this.submitRow}
                    caregivers={this.state.allCaregivers}
                />
            </div>
                <div>
                    <EditMedicationPlan
                        showMedicationPlan={this.state.showMedicationPlan}
                        hideModal={this.hideMedicationPlan}
                        selectedRow={this.state.selectedRow}
                        intakes={this.state.intakes}

                    />
                </div>
            <Col>
                <DeletePopUp
                    showDeletePopUp={this.state.showDeletePopUp}
                    hideDeletePopUp={this.hideDeletePopUp}
                    deleteRow={this.deleteRow}
                />
            </Col>
        </div>)
    }
}

export default MyPatientsAsDoctor
