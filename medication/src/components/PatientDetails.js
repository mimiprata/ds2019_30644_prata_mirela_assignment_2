import {Button, Col, ControlLabel, FormControl, Modal, OverlayTrigger, Row, Tooltip} from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";
import React from "react";

const PatientDetails = (props) => {
    const { showModal, hideModal, handleChange, handleDate, submitRow,  caregivers} = props;
    const { username, password, name, gender, birthDate, address, medicalRecord} = props.selectedRow;
    return(<Modal
        show={showModal}
        onHide={hideModal}
        aria-labelledby="contained-modal-title-lg"
        dialogClassName="custom-modal-caregivers"
    >
        <Modal.Header className="modal-header-caregivers">
            <Modal.Title className="modal-title-caregivers">Patient Details</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body-caregivers">
            {props.showModalForAdd=== true ?
                <div>
                    <Row>
                        <Col xs={6}>
                            <ControlLabel>Username</ControlLabel>
                        </Col>
                        <Col xs={6}>
                            <FormControl
                                name="username"
                                type="text"
                                onChange={handleChange}
                                value={username}
                            />
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col xs={6}>
                            <ControlLabel>Password</ControlLabel>
                        </Col>
                        <Col xs={6}>
                            <FormControl
                                name="password"
                                type="password"
                                onChange={handleChange}
                                value={password}
                            />
                        </Col>
                    </Row>
                </div>
                : null
            }
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Name</ControlLabel>
                </Col>
                <Col xs={6}>
                    <FormControl
                        name="name"
                        type="text"
                        onChange={handleChange}
                        value={name}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Date</ControlLabel>
                </Col>
                <Col xs={6}>
                    <DatePicker
                        showTodayButton={false}
                        dateFormat="YYYY-MM-DD"
                        weekStartsOn={1}
                        value={birthDate}
                        onChange={handleDate}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Gender</ControlLabel>
                </Col>
                <Col xs={6}>

                    <FormControl
                        name="gender"
                        componentClass="select"
                        onChange={handleChange}
                        value={gender}
                    >
                        <option>M</option>
                        <option>F</option>
                    </FormControl>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Address</ControlLabel>
                </Col>
                <Col xs={6}>
                    <FormControl
                        name="address"
                        type="text"
                        onChange={handleChange}
                        value={address}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Medical Record</ControlLabel>
                </Col>
                <Col xs={6}>
                    <FormControl
                        name="medicalRecord"
                        type="text"
                        onChange={handleChange}
                        value={medicalRecord}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Caregiver</ControlLabel>
                </Col>
                <Col xs={6}>
            <FormControl
                name="idCaregiver"
                componentClass="select"
                onChange={handleChange}

            >
                {
                    caregivers && caregivers.map((item)=><option
                        key={item.id} value={item.id}>{item.name}</option>)
                }

            </FormControl>
                </Col>
            </Row>

            <Row>
                <div className="buttons-modal-caregivers">
                    <OverlayTrigger
                        overlay={<Tooltip id="info-update">Update</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-save-caregivers"
                            bsStyle={null}
                            type="submit"
                            onClick={(e) => {
                                submitRow(e);
                            }}
                        >
                            <i className="glyphicon glyphicon-floppy-disk"/>
                        </Button>
                    </OverlayTrigger>
                    <div>&nbsp;&nbsp;</div>
                    <OverlayTrigger
                        overlay={<Tooltip id="info-return">Return</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-left-caregivers"
                            bsStyle={null}
                            onClick={(e) => {
                                hideModal(e);
                            }}>
                            <i className="glyphicon glyphicon-arrow-left"/>
                        </Button>
                    </OverlayTrigger>
                </div>
            </Row>

        </Modal.Body>
    </Modal>);

};

export default PatientDetails;