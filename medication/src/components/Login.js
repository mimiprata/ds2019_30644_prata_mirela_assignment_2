import React, {Component} from 'react';
import {Panel} from 'react-bootstrap'
import LoginForm from "./atoms/LoginForm";
import axios from 'axios';



class Login extends Component{
    constructor(props) {
        super(props);
        this.state={
            username:'',
            password:'',
            role:''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e){
        e.preventDefault();

        const user = {
            username: this.state.username,
            password: this.state.password,
            role: ''
        };

        console.log(user);

        axios.post(`http://localhost:8080/login`,  user)
            .then(res => {
                console.log(res.data);
                localStorage.setItem('username',res.data.username);
                localStorage.setItem('role',res.data.role);
                    this.props.history.push('/home')
            });

    }

    render() {
        return(
        <Panel style={{padding:'30px', width:'50%'}}>
            <LoginForm
                username={this.state.username}
                password={this.state.password}
                handleChange={this.handleChange}
                handleSubmit={this.handleSubmit}
            />

        </Panel>)
    }

}

export default Login;