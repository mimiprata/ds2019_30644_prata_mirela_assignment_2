import React, {Component} from 'react';
import {Panel, Row, Col, Alert, Button} from 'react-bootstrap'
import axios from 'axios';
import NavBar from './atoms/NavBar'
import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";
import PanelTitle from "react-bootstrap/es/PanelTitle";

class Home extends Component {


    constructor(props) {
        super(props);
        this.state = {
            patient:'',
            caregiver: '',
            doctor: '',
            listOfAlerts : [],
        }
    }

    componentWillMount() {
        console.log(localStorage.getItem('username'));
        if (localStorage.getItem('role') === 'PATIENT') {
            axios.post(`http://localhost:8080/patientProfile`, {username: localStorage.getItem('username')})
                .then(res => {
                    console.log(res.data);
                    localStorage.setItem('idPatient', res.data.id);
                    this.setState({patient: res.data})
                }).then(res => {
                    axios.post(`http://localhost:8080/getMyCaregiver`, {id: localStorage.getItem('idPatient')})
                        .then(res => {
                            console.log(res.data);
                            this.setState({caregiver: res.data})
                        });
                    axios.post(`http://localhost:8080/getMyDoctor`, {id: localStorage.getItem('idPatient')})
                        .then(res => {
                            console.log(res.data);
                            this.setState({doctor: res.data})
                        });
                }
            );
        } else if (localStorage.getItem('role') === 'CAREGIVER'){
            axios.post(`http://localhost:8080/caregiver/profile`, {username: localStorage.getItem('username')} )
                .then(res => {
                    console.log(res.data);
                    localStorage.setItem('idCaregiver', res.data.id);
                    this.setState({caregiver: res.data})
                }).then(()=> {axios.post(`http://localhost:8080/caregiver/myDoctor`, {id: localStorage.getItem('idCaregiver')})
                .then(res => {
                    console.log(res.data);
                    this.setState({doctor: res.data})
                });}
                );
            let ws = new SockJS('http://localhost:8080/gs-guide-websocket');
            let stompClient = Stomp.over(ws);
            const _this = this;
            let list = [];
            stompClient.connect({}, function (frame) {
                stompClient.subscribe("/topic", function (sdkEvent) {
                    console.log(sdkEvent.body);

                    //list.push(JSON.parse(sdkEvent.body));
                    list = [JSON.parse(sdkEvent.body), ...list];
                    _this.setState({listOfAlerts:list});
                });
            },  null);
        } else if (localStorage.getItem('role') === 'DOCTOR'){
            axios.post(`http://localhost:8080/doctor/profile`, {username: localStorage.getItem('username')} )
                .then(res => {
                    console.log(res.data);
                    localStorage.setItem('idDoctor', res.data.id);
                    this.setState({caregiver: res.data})
                });
        }
    }

    getPeriod =(activity,end, start)=>{
        const endM = new Date(end).getTime();
        const startM = new Date(start).getTime();
        const diff = endM-startM;
        const  diffHours = Math.trunc(diff / (60 * 60 * 1000));
        const diffMinutes = Math.trunc(diff / (60 * 1000));
        if (activity==='Toileting\t'){
            return `${diffMinutes} minutes`
        }
        else return `${diffHours} hours`
    }



    render() {
        console.log(localStorage.getItem('role'));
        console.log(this.state.listOfAlerts);
        return (<div>
            <NavBar/>
            <Col xs={2}/>
            <Col xs={8}>
            <Alert bsStyle="success" >
                <strong>Holy guacamole!</strong>
                {localStorage.getItem('role') === "PATIENT" ?
                    <div>Best check yo self, you're not looking too good.</div>:
                    null
                }
            </Alert>
            {

                localStorage.getItem('role') === "PATIENT" ?
                    <div>
                        <Col xs={3}>
                            <Col xs={12}>
                        <b>
                            My Caregiver
                        </b>
                        <br/>
                        {this.state.caregiver.name}
                        <br/>
                            </Col>
                        </Col>
                        <Col xs={3}>
                            <Col xs={12}>
                        <b>
                            My Doctor
                        </b>
                        <br/>
                        {this.state.doctor.name}
                    </Col>
                    </Col>
                    </div> :
                    localStorage.getItem('role') === "CAREGIVER" ?
                        <div>
                            <b>
                                My Doctor
                            </b>
                            <br/>
                            {this.state.doctor.name}

                            <br/>
                            <hr/>
                            {
                                this.state.listOfAlerts.length===0 ?
                                    <div>No notifications yet!</div>
                                    :
                                this.state.listOfAlerts.map((item,index)=>{

                                    if(localStorage.getItem('idCaregiver')=== item.idCaregiver.toString()) {
                                        return (<Panel  key={index} bsStyle="danger">
                                            <Panel.Heading>
                                                <Panel.Title componentClass="h3">{item.activity}
                                                </Panel.Title>
                                            </Panel.Heading>
                                            <Panel.Body>
                                                <Row>
                                                <Col xs={6}>
                                                Your patient {item.patientName} activity took place for more than {this.getPeriod(item.activity, item.end, item.start)}!
                                                </Col>
                                                    <Col xs={6}>
                                                Start Date : {item.start}
                                                <br/>
                                                End Date  : {item.end}
                                                    </Col>
                                                </Row>
                                            </Panel.Body>
                                        </Panel>)
                                    }
                                    else return <div>No notifications yet!</div>;
                                })

                            }
                        </div> :
                        null
            }
            </Col>
            <Col xs={2}/>
        </div>)
    }

}

export default Home;

