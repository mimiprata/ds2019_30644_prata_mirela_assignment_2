import React, {Component} from 'react';
import {
    Modal, Col, Row, ControlLabel, FormControl, FormGroup,
    Button, Form, OverlayTrigger, Tooltip
} from 'react-bootstrap';
import './MedicationDetails.scss'

const CaregiverDetails = (props) => {
    const { showModal, hideModal, handleChange, submitRow} = props;
    const { name, description} = props.selectedRow;
    return(<Modal
        show={showModal}
        onHide={hideModal}
        aria-labelledby="contained-modal-title-lg"
        dialogClassName="custom-modal-medication"
    >
        <Modal.Header className="modal-header-medication">
            <Modal.Title className="modal-title-medication">Medication Details</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body-medication">
                <div>
                    <Row>
                        <Col xs={6}>
                            <ControlLabel>Name</ControlLabel>
                        </Col>
                        <Col xs={6}>
                            <FormControl
                                name="name"
                                type="text"
                                onChange={handleChange}
                                value={name}
                            />
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col xs={6}>
                            <ControlLabel>Description</ControlLabel>
                        </Col>
                        <Col xs={6}>
                            <FormControl
                                name="description"
                                type="text"
                                onChange={handleChange}
                                value={description}
                            />
                        </Col>
                    </Row>
                </div>
              
           

            <Row>
                <div className="buttons-modal-medication">
                    <OverlayTrigger
                        overlay={<Tooltip id="info-update">Update</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-save-medication"
                            bsStyle={null}
                            type="submit"
                            onClick={(e) => {
                                submitRow(e);
                            }}
                        >
                            <i className="glyphicon glyphicon-floppy-disk"/>
                        </Button>
                    </OverlayTrigger>
                    <div>&nbsp;&nbsp;</div>
                    <OverlayTrigger
                        overlay={<Tooltip id="info-return">Return</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-left-medication"
                            bsStyle={null}
                            onClick={(e) => {
                                hideModal(e);
                            }}>
                            <i className="glyphicon glyphicon-arrow-left"/>
                        </Button>
                    </OverlayTrigger>
                </div>
            </Row>

        </Modal.Body>
    </Modal>);

};

export default CaregiverDetails;