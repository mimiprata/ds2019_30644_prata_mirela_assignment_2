import React, {Component} from 'react';

import { Redirect} from "react-router-dom";



class Logout extends Component {

    constructor(props) {
        super(props);
        this.state= {
            redirect:''
        }
    }
    componentWillMount() {
        localStorage.clear();
        this.setState({redirect:true})
    }
    render() {
        console.log(this.state);
        if(this.state.redirect === true){
          return   <Redirect to="/"/>
        }
    }
};
export default Logout;