import React, {Component} from 'react';
import axios from "axios";
import NavBar from "./atoms/NavBar";
import {Button, Col, ControlLabel, FormControl, Row, Form} from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";


class MyAccount extends Component{
    constructor(props) {
        super(props);
        this.state={
            patient:'',
            disabled: true,
        }
    }

    componentWillMount() {
        if (localStorage.getItem('role')=== 'PATIENT' ){
            axios.post(`http://localhost:8080/patientProfile`, {username: localStorage.getItem('username')} )
                .then(res => {
                    console.log(res.data);
                    localStorage.setItem('idPatient',res.data.id);
                    this.setState({patient: res.data})
                });
        }

        if (localStorage.getItem('role')==='CAREGIVER'){
            axios.post(`http://localhost:8080/caregiver/profile`, {username: localStorage.getItem('username')} )
                .then(res => {
                    console.log(res.data);
                    localStorage.setItem('idCaregiver',res.data.id);
                    this.setState({patient: res.data})
                });
        }
    }

    handleChange = (event) =>{
        const target = event.target;
        debugger;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let patient = {...this.state.patient};
        patient[name] = value;
        this.setState({patient: patient})
};

    handleDate = (value) => {
        let row1, data;
        if (value) data = value.split("T")[0];
        row1 = {...this.state.patient};
        row1['birthDate'] = data;
        this.setState({patient: row1});
    };

    startEdit = () =>{
        this.setState({disabled:false});
    };

    saveChanges = ()=>{
        this.setState({disabled:true});

        if(localStorage.getItem('role')==='PATIENT') {
            axios.post(`http://localhost:8080/updatePatient`, this.state.patient)
                .then(res => {
                    console.log(res.data);
                    this.setState({patient: res.data})
                });
        }
        else {
            axios.post(`http://localhost:8080/caregiver/update`, this.state.patient)
                .then(res => {
                    console.log(res.data);
                    this.setState({patient: res.data})
                });
        }

    };

    render() {
        console.log(this.state.patient);
        console.log(JSON.stringify(this.state.patient));
        return (
            <div>
                <NavBar/>
                <Col xs={2}/>
                <Col xs={8}>
                    <Form>
                       <ControlLabel>
                           Name
                       </ControlLabel>
                        <FormControl
                            type="text"
                            name={"name"}
                            disabled={this.state.disabled}
                            value={this.state.patient.name}
                            onChange={(e)=>{this.handleChange(e)}}/>
                        <ControlLabel>Date</ControlLabel>

                        <DatePicker
                            showTodayButton={false}
                            dateFormat="YYYY-MM-DD"
                            weekStartsOn={1}
                            disabled={this.state.disabled}
                            value={this.state.patient.birthDate}
                            onChange={this.handleDate}
                        />

                            <ControlLabel>Gender</ControlLabel>


                            <FormControl
                                name="gender"
                                componentClass="select"
                                onChange={this.handleChange}
                                disabled={this.state.disabled}
                                value={this.state.patient.gender}
                            >
                                <option>M</option>
                                <option>F</option>
                            </FormControl>
                        <ControlLabel>
                            Address
                        </ControlLabel>
                        <FormControl
                            type="text"
                            name={"address"}
                            disabled={this.state.disabled}
                            value={this.state.patient.address}
                            onChange={(e)=>{this.handleChange(e)}}/>

                        {
                            localStorage.getItem('role') ==='PATIENT'?
                                <div>
                            <ControlLabel>
                                Medical Record
                            </ControlLabel>
                            < FormControl
                            type="text"
                            name={"medicalRecord"}
                            disabled={this.state.disabled}
                            value={this.state.patient.medicalRecord}
                            onChange={(e)=>{this.handleChange(e)}}/>
                            </div>
                                :null
                        }


                    </Form>
                    <br/>
                   <Row>
                       <Col xs={12}>
                       {
                           this.state.disabled ?
                               < Button bsStyle="primary" onClick={this.startEdit}>Edit</Button> :
                               <Button bsStyle="success" onClick={this.saveChanges}>Save</Button>
                       }
                       </Col>
                   </Row>
                </Col>
                <Col xs={2}/>
            </div>
        );
    }

};

export default MyAccount;