import React, {Component} from 'react';
import axios from "axios";
import {Button, ButtonGroup, ButtonToolbar, Col, OverlayTrigger, Panel, Row, Tooltip, Modal} from "react-bootstrap";
import NavBar from "./atoms/NavBar";
import './EditMedicationPlan.scss'
import IntakeTable from "./IntakeTable";
import IntakeDetails from "./IntakeDetails";
import DeletePopUp from "./atoms/DeletePopUp";

class EditMedicationPlan extends Component{
    constructor(props) {
        super(props);
        this.state = {
            intakes: '',
            medications: '',
            modalForAdd: false,
            showModal: false,
            selectedRow: '',
            showDeletePopUp: false
        };
        this.options = {
            sortIndicator: true, // disable sort indicator

            sizePerPageList: [
                {
                    text: '10',
                    value: 10
                },
                {
                    text: '25',
                    value: 25
                },
                {
                    text: '50',
                    value: 50
                },
                {
                    text: 'All',
                    value: 300
                }
            ], //pagination options
            sizePerPage: 50, // Which size per page to locate as default
            pageStartIndex: 1, // Where to start counting the pages
            paginationSize: 3, // The pagination bar size.
            prePage: '<', // Previous page button text
            nextPage: '>', // Next page button text
            firstPage: '<<', // First page button text
            lastPage: '>>', // Last page button text
            paginationShowsTotal: this.renderShowsTotal, //implemented
            paginationPanel: this.renderPaginationPanel, //implemented
            onRowClick: this.showModalForRow
        };
    }
    componentDidMount() {
        console.log(this.props.selectedRow.id);
        /*let patientDTO = {
            id:"7",
            name: 'Mimi',
            birthDate: '2018-09-09',
            gender: 'F',
            address: 'adr1',
            medicalRecord:'none'
        };
        let userDTO = {
            username: 'p1',
            password: 'p1'
        };


        if (localStorage.getItem('role') === 'DOCTOR')
        {
            //axios.post(`http://localhost:8080/doctor/updatePatient`,  patientDTO, {params: {idCaregiver:2}}  );
            axios.get(`http://localhost:8080/doctor/myPatients`,  )
                .then(res => {
                    console.log(res.data);
                    this.setState({medication-plan: res.data})
                });
            axios.get(`http://localhost:8080/doctor/allCaregivers`).then(res=>{
                console.log(res.data);
                this.setState({allCaregivers: res.data})
            })
        }*/

        if (localStorage.getItem('role') === 'DOCTOR') {

            axios.get(`http://localhost:8080/doctor/allMedications`).then(res => {
                this.setState({medications: res.data})
            })
        }
    }

    /*componentDidUpdate(prevProps, prevState, snapshot) {
        if(prevProps.selectedRow !== this.props.selectedRow){
            if (localStorage.getItem('role')=== 'DOCTOR'){
                axios.post(`http://localhost:8080/getIntakes`, {id: this.props.selectedRow.id })
                    .then(res => {
                        console.log(res.data);
                        this.setState({intakes: res.data})
                    });
                axios.get(`http://localhost:8080/doctor/allMedications`).then(res=>{
                    this.setState({medications:res.data})
                })
            }
        }
    }*/

    showDeletePopUp =()  =>{
        this.setState({showDeletePopUp: true});
    };

    deleteRow = () =>{
        axios.delete(`http://localhost:8080/doctor/deleteIntakeToAPatient`,{params:{id: this.state.selectedRow.id}})
            .then(()=>{
                axios.post(`http://localhost:8080/getIntakes`,{id:this.props.selectedRow.id} )
                    .then(res => {
                        console.log(res.data);
                        this.setState({intakes:res.data})
                    });
            });
        this.hideDeletePopUp();
    };

    hideDeletePopUp = () =>{
        this.setState({showDeletePopUp: false});
    };

    showModalForRow = (row, columnIndex, rowIndex) => {
        console.log(columnIndex);
        this.setState({
            selectedRow: row,})
        if (columnIndex !==6 && columnIndex!== undefined){
            this.setState({showModal:true})
        };
    };

    showModalForAdd = () =>{
        this.setState({
            showModal: true,
            modalForAdd: true,
            selectedRow: {
                startDate:'', endDate:'', details:'',idMedication: '1'
            }
        });
    };

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;
        let row1;
        row1 = {...this.state.selectedRow};
        row1[name] = value;
        this.setState({selectedRow: row1});
    };

    handleStartDate = (value) => {
        let row1, data;
        if (value) data = value.split("T")[0];
        row1 = {...this.state.selectedRow};
        row1['startDate'] = data;
        this.setState({selectedRow: row1});
    };
    handleEndDate = (value) => {
        let row1, data;
        if (value) data = value.split("T")[0];
        row1 = {...this.state.selectedRow};
        row1['endDate'] = data;
        this.setState({selectedRow: row1});
    };

    hideModal =() =>{
        this.setState({showModal: false, modalForAdd: false});
        // this.clearErrorMessage();
    };

    validationBeforeSubmit = () =>{
        return true;
    };

    submitRow = (e) => {

        let userDTO = {
            username: this.state.selectedRow.username,
            password: this.state.selectedRow.password
        };

        /*let caregiverDTO = {
            id: this.state.selectedRow.id,
            name: this.state.selectedRow.name,
            birthDate: this.state.selectedRow.birthDate,
            gender: this.state.selectedRow.gender,
            address: this.state.selectedRow.address
        }*/
        /*   console.log(JSON.stringify({
               patientDTONew,
               userDTO
           }));*/

        e.preventDefault();
        if (this.validationBeforeSubmit()) {
            //this.clearErrorMessage();
            if (this.state.modalForAdd) {   //if a row is added
                 let intakeDTO = {
                    startDate: this.state.selectedRow.startDate,
                    endDate: this.state.selectedRow.endDate,
                    details: this.state.selectedRow.details
                };
                console.log(intakeDTO);
                console.log(this.state.selectedRow.idMedication);
                console.log(localStorage.getItem('idP'))
                 axios.post(`http://localhost:8080/doctor/addIntakeToAPatient`, intakeDTO,{params:{ idPatient:this.props.selectedRow.id, idMedication:this.state.selectedRow.idMedication}}).then(
                     () => {
                         axios.post(`http://localhost:8080/getIntakes`, {id: this.props.selectedRow.id} )
                             .then(res => {
                                 console.log(res.data);
                                 this.setState({intakes: res.data})
                             });
                     }
                 );
                this.hideModal();
            } else {
            let intakeDTO = {
                id: this.state.selectedRow.id,
                startDate: this.state.selectedRow.startDate,
                endDate: this.state.selectedRow.endDate,
                details: this.state.selectedRow.details
            };
            console.log(intakeDTO);
            console.log(this.state.selectedRow.idMedication);
            console.log(localStorage.getItem('idP'))
            axios.post(`http://localhost:8080/doctor/updateIntakeToAPatient`, intakeDTO,{params:{ idPatient:this.props.selectedRow.id, idMedication:this.state.selectedRow.idMedication}}).then(
                () => {
                    axios.post(`http://localhost:8080/getIntakes`, {id: this.props.selectedRow.id} )
                        .then(res => {
                            console.log(res.data);
                            this.setState({intakes: res.data})
                        });
                }
            );
                this.hideModal();

        }}};





    renderShowsTotal(start, to, total) {
        return <p style={{color: 'blue', marginTop: '10px'}}>Total: {total}&nbsp;&nbsp;</p>;
    }

    renderPaginationPanel = (props) =>{
        return (
            <div className="pagination-style-medication-plan">
                <div className="pagination-style-medication-plan-dropDownMenu">
                    {props.components.totalText}
                    {props.components.sizePerPageDropdown}
                </div>
                <div className="pagination-list-medication-plan">
                    <ButtonToolbar className="table-buttons-container-medication-plan">
                        <ButtonGroup bsClass="add-delete-print-buttons-medication-plan">
                            <OverlayTrigger
                                overlay={<Tooltip id="info-table">Add Caregiver</Tooltip>}
                                placement="bottom"
                            >
                                <Button bsStyle="success" className="button-add-medication-plan"
                                        onClick={this.showModalForAdd}>
                                    <i className="glyphicon glyphicon-plus"/>
                                </Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </ButtonToolbar>
                    {props.components.pageList}
                </div>
            </div>
        );
    }
        render(){
        console.log(this.state.intakes);
            console.log(this.props.selectedRow.id);
            console.log(this.props.intakes);
        return(
            <div>
            <Modal
                show={this.props.showMedicationPlan}
                onHide={this.props.hideModal}
                aria-labelledby="contained-modal-title-lg"
                bsSize="lg"
            >

                    <Modal.Body>

                    <Panel className="medication-plan-panel-container">
                        <Panel.Heading className="medication-plan-panel-header">
                            <Row className="medication-plan-panel-header-row">
                                <Col xs={11} className={'title-col-medication-plan'}>
                                    <p><b> Medication Plan for patient { this.props.selectedRow && this.props.selectedRow.name}</b></p>
                                </Col>
                                <Col xs={1} className="info-col-medication-plan">
                                    <OverlayTrigger
                                        placement="bottom"
                                        overlay={<Tooltip id="info-table">Click on the row to view
                                            details</Tooltip>}
                                    >
                                        <i className={'glyphicon glyphicon-info-sign'}/>
                                    </OverlayTrigger>
                                </Col>
                            </Row>
                        </Panel.Heading>
                        <Panel.Body>
                            {
                                this.state.intakes!== '' ?
                                <IntakeTable
                                    intakes={this.state.intakes}
                                    options={this.options}
                                    showDeletePopUp={this.showDeletePopUp}

                                />:

                                    <IntakeTable
                                        intakes={this.props.intakes}
                                        options={this.options}
                                        showDeletePopUp={this.showDeletePopUp}

                                    />

                            }
                        </Panel.Body>
                    </Panel>


                    </Modal.Body>

            </Modal>
            <div>
            <IntakeDetails
            showModal={this.state.showModal}
            showModalForAdd={this.state.modalForAdd}
            hideModal={this.hideModal}
            handleStartDate={this.handleStartDate}
            handleEndDate={this.handleEndDate}
            handleChange={this.handleChange}
            selectedRow={this.state.selectedRow}
            submitRow={this.submitRow}
            medications={this.state.medications}
            />
        </div>
        <Col>
        <DeletePopUp
        showDeletePopUp={this.state.showDeletePopUp}
        hideDeletePopUp={this.hideDeletePopUp}
        deleteRow={this.deleteRow}
        />
        </Col>
            </div>
        )
        
        }

};

export default EditMedicationPlan;