import {Button, Col, ControlLabel, FormControl, Modal, OverlayTrigger, Row, Tooltip} from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";
import React from "react";

const IntakeDetails = (props) => {
    const { showModal, hideModal, handleChange, handleStartDate, handleEndDate,  submitRow,  medications} = props;
    const { startDate, endDate, details} = props.selectedRow;
    return(<Modal
        show={showModal}
        onHide={hideModal}
        aria-labelledby="contained-modal-title-lg"
        dialogClassName="custom-modal-caregivers"
    >
        <Modal.Header className="modal-header-caregivers">
            <Modal.Title className="modal-title-caregivers">Intake Details</Modal.Title>
        </Modal.Header>
        <Modal.Body className="modal-body-caregivers">
            <Row>
                <Col xs={6}>
                    <ControlLabel>Medication</ControlLabel>
                </Col>
                <Col xs={6}>
                    <FormControl
                        name="idMedication"
                        componentClass="select"
                        onChange={handleChange}

                    >
                        {
                            medications && medications.map((item)=><option
                                key={item.id} value={item.id}>{item.name}</option>)
                        }

                    </FormControl>
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Start Date</ControlLabel>
                </Col>
                <Col xs={6}>
                    <DatePicker
                        showTodayButton={false}
                        dateFormat="YYYY-MM-DD"
                        weekStartsOn={1}
                        value={startDate}
                        onChange={handleStartDate}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>End Date</ControlLabel>
                </Col>
                <Col xs={6}>
                    <DatePicker
                        showTodayButton={false}
                        dateFormat="YYYY-MM-DD"
                        weekStartsOn={1}
                        value={endDate}
                        onChange={handleEndDate}
                    />
                </Col>
            </Row>
            <br/>
            <Row>
                <Col xs={6}>
                    <ControlLabel>Details</ControlLabel>
                </Col>
                <Col xs={6}>
                    <FormControl
                        name="details"
                        type="text"
                        onChange={handleChange}
                        value={details}
                    />
                </Col>
            </Row>
            <Row>
                <div className="buttons-modal-caregivers">
                    <OverlayTrigger
                        overlay={<Tooltip id="info-update">Update</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-save-caregivers"
                            bsStyle={null}
                            type="submit"
                            onClick={(e) => {
                                submitRow(e);
                            }}
                        >
                            <i className="glyphicon glyphicon-floppy-disk"/>
                        </Button>
                    </OverlayTrigger>
                    <div>&nbsp;&nbsp;</div>
                    <OverlayTrigger
                        overlay={<Tooltip id="info-return">Return</Tooltip>}
                        placement="bottom">
                        <Button
                            className="button-left-caregivers"
                            bsStyle={null}
                            onClick={(e) => {
                                hideModal(e);
                            }}>
                            <i className="glyphicon glyphicon-arrow-left"/>
                        </Button>
                    </OverlayTrigger>
                </div>
            </Row>


        </Modal.Body>
    </Modal>);

};

export default IntakeDetails;