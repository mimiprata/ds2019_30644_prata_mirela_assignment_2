import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Login from "./components/Login";
import MyAccount from "./components/MyAccount"
import Home from './components/Home'
import Logout from './components/Logout'
import MedicationPlan from './components/MedicationPlan'
import MyPatients from './components/Caregiver/MyPatients'
import MyCaregivers from './components/MyCaregivers'
import MyPatientsAsDoctor from './components/MyPatientsAsDoctor'
import Medication from './components/Medication'
import EditMedicationPlan from './components/EditMedicationPlan'
import {DoctorRoute} from "./DoctorRoute";
import {PatientRoute} from "./PatientRoute";
import {CaregiverRoute} from "./CaregiverRoute";
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

function f() {
   if( localStorage.getItem('role')==='CAREGIVER')  return MyPatients;
   else return MyPatientsAsDoctor
};
class App extends React.Component {

   /* componentDidMount() {
        let ws = new SockJS('http://localhost:8080/gs-guide-websocket');
        let stompClient = Stomp.over(ws);
        const _this = this;
        stompClient.connect({}, function (frame) {
            stompClient.subscribe("/topic", function (sdkEvent) {
                console.log(sdkEvent);
            });
        }, null);
    }*/

    render() {
    return (
        <Router>
          <div>
            <Route exact path="/" component={Login}/>
            <Route path="/home" component={Home}/>
            <Route path="/myAccount" component={MyAccount}/>
            <Route path="/logout" component={Logout}/>
            <PatientRoute exact path="/myMedicationPlan" component={MedicationPlan}/>
            <DoctorRoute path="/patients" component={MyPatientsAsDoctor}/>
              <CaregiverRoute exact path="/myPatients" component={MyPatients}/>
              <DoctorRoute exact path="/myCaregivers" component={MyCaregivers}/>
            <DoctorRoute exact path="/medication" component={Medication}/>
              <DoctorRoute exact path="/editMedicationPlan" component={EditMedicationPlan}/>
          </div>
        </Router>
    )
  }
}

export default App;
