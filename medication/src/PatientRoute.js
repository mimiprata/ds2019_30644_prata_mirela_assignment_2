import React from 'react';
import {Redirect, Route} from "react-router-dom";

export const PatientRoute =({component: Component, ...rest}) =>(
    <Route
        {...rest}
        render={props=>
            localStorage.getItem('role')==='PATIENT'?
                (<Component {...props}/>):
                (<Redirect to={{pathname:"/", state:{from: props.location}}}/>)
        }
    />
);