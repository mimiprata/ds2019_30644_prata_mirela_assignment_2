import React from 'react';
import {Redirect, Route} from "react-router-dom";

export const DoctorRoute =({component: Component, ...rest}) =>(
    <Route
        {...rest}
        render={props=>
        localStorage.getItem('role')==='DOCTOR'?
            (<Component {...props}/>):
            (<Redirect to={{pathname:"/", state:{from: props.location}}}/>)
        }
    />
);