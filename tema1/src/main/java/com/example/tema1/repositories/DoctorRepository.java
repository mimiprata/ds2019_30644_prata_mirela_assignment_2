package com.example.tema1.repositories;

import com.example.tema1.entities.Doctor;
import com.example.tema1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {
    public Doctor findByUser(User user);
}
