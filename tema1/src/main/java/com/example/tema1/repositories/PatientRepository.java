package com.example.tema1.repositories;

import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Patient;
import com.example.tema1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    public Patient findByUser(User user);
    public List<Patient> findByCaregiver(Caregiver caregiver);
}
