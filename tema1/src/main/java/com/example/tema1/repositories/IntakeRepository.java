package com.example.tema1.repositories;

import com.example.tema1.entities.Intake;
import com.example.tema1.entities.Medication;
import com.example.tema1.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IntakeRepository extends JpaRepository<Intake, Integer> {
    public List<Intake> findByPatient(Patient patient);
    public Intake findByMedication(Medication medication);
}
