package com.example.tema1.repositories;

import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Doctor;
import com.example.tema1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {
    public Caregiver findByUser(User user);
    public List<Caregiver> findByDoctor(Doctor doctor);
}
