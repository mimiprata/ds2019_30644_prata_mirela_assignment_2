package com.example.tema1;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;


public class CustomMessage {

    private String patient_id;
    private String activity;
    private Timestamp startTime;
    private Timestamp endTime;

    public CustomMessage(@JsonProperty("patient_id") String patient_id,
                         @JsonProperty("activity") String activity,
                         @JsonProperty("startTime") Timestamp startTime,
                         @JsonProperty("endTime") Timestamp endTime) {
        this.patient_id=patient_id;
        this.activity=activity;
        this.startTime=startTime;
        this.endTime=endTime;

    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "CustomMessage{" +
                "patient_id='" + patient_id + '\'' +
                ", activity='" + activity + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}

