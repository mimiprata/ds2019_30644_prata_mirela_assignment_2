package com.example.tema1;

public class MonitoredData {
    private String x;

    public MonitoredData() {
    }

    public MonitoredData(String x) {
        this.x = x;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }
}
