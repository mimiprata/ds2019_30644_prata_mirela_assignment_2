package com.example.tema1.controllers;


import com.example.tema1.dto.*;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@Controller
@RequestMapping(value="/doctor")
public class DoctorController {

    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private PatientService patientService;
    @Autowired
    private MedicationService medicationService;

    @Autowired
    private IntakeService intakeService;

    @PostMapping(value = "/profile")
    public ResponseEntity<?> getCaregiverByUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            DoctorDTO doctorDTO = doctorService.findByUser(user);
            return new ResponseEntity<>(doctorDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/myCaregivers")
    public ResponseEntity<?> getMyCaregivers(@RequestBody DoctorDTO doctorDTO) {
        try {
            List<CaregiverDTO> doctorDTOList = caregiverService.findCaregiversByDoctor(doctorDTO);
            return new ResponseEntity<>(doctorDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping(value = "/addCaregiver")
    public ResponseEntity<?> addCaregiver(@RequestParam Integer idDoctor, @RequestBody NewCaregiverDTO newCaregiverDTO) {
        try {
            caregiverService.addCaregiverToDoctor(newCaregiverDTO.getCaregiverDTO(),newCaregiverDTO.getUserDTO(),idDoctor);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/updateCaregiver")
    public ResponseEntity<?> updateCaregiver(@RequestParam Integer idDoctor, @RequestBody CaregiverDTO caregiverDTO) {
        try {
            caregiverService.updateCaregiverWithDoctor(caregiverDTO,idDoctor);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }


    @DeleteMapping(value="/deleteCaregiver")
    public ResponseEntity<?> deleteCaregiver( @RequestParam Integer id) {
        try {
            caregiverService.deleteCaregiverById(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/myPatients")
    public ResponseEntity<?> getMyPatients() {
        try {
            List<PatientDTO> patientDTOList = patientService.findAllPatients();
            return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/allCaregivers")
    public ResponseEntity<?> getCaregivers() {
        try {
            List<CaregiverDTO> caregiverDTOList = caregiverService.findAllCaregivers();
            return new ResponseEntity<>(caregiverDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/addPatient")
    public ResponseEntity<?> addPatient(@RequestParam Integer idCaregiver, @RequestBody NewPatientDTO newPatientDTO) {
        try {
            patientService.addPatientToCaregiver(newPatientDTO.getPatientDTO(),newPatientDTO.getUserDTO(),idCaregiver);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/updatePatient")
    public ResponseEntity<?> updatePatient(@RequestParam Integer idCaregiver, @RequestBody PatientDTO patientDTO) {
        try {
            patientService.updatePatientWithCaregiver(patientDTO,idCaregiver);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value="/deletePatient")
    public ResponseEntity<?> deletePatient( @RequestParam Integer id) {
        try {
            patientService.deletePatientById(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/allMedications")
    public ResponseEntity<?> getMedications() {
        try {
            List<MedicationDTO> medicationDTOList = medicationService.findAllMedications();
            return new ResponseEntity<>(medicationDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(value = "/addMedication")
    public ResponseEntity<?> addMedication( @RequestBody MedicationDTO medicationDTO) {
        try {
            medicationService.addMedication(medicationDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/updateMedication")
    public ResponseEntity<?> updateMedication( @RequestBody MedicationDTO medicationDTO) {
        try {
            medicationService.updateMedication(medicationDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value="/deleteMedication")
    public ResponseEntity<?> deleteMedication(@RequestParam Integer id){
        try {
            medicationService.deleteMedicationById(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/addIntakeToAPatient")
    public ResponseEntity<?> addIntakeToAPatient (@RequestParam Integer idPatient, @RequestParam Integer idMedication,  @RequestBody IntakeDTO intakeDTO) {
        try {
            intakeService.addInttakeToAPatient(idPatient, idMedication, intakeDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/updateIntakeToAPatient")
    public ResponseEntity<?> updateIntakeToAPatient (@RequestParam Integer idPatient, @RequestParam Integer idMedication,  @RequestBody IntakeDTO intakeDTO) {
        try {
            intakeService.updateIntakeToAPatient(idPatient, idMedication, intakeDTO);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(value="/deleteIntakeToAPatient")
    public ResponseEntity<?> deleteIntakeToAPatient (@RequestParam Integer id) {
        try {
            intakeService.deleteIntakeById(id);
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value="/allDoctors")
    public ResponseEntity<?> getAllDoctors() {
        try {
           List<DoctorDTO> doctorDTOList=doctorService.findAllDoctors();
            return new ResponseEntity<>(doctorDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }





}
