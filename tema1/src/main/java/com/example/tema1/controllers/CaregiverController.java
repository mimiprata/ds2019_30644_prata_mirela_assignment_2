package com.example.tema1.controllers;

import com.example.tema1.dto.CaregiverDTO;
import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.dto.PatientDTO;
import com.example.tema1.dto.UserDTO;
import com.example.tema1.dto.mapper.UserMapper;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.User;
import com.example.tema1.services.CaregiverService;
import com.example.tema1.services.PatientService;
import com.example.tema1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@CrossOrigin("http://localhost:3000")
@Controller
@RequestMapping(value="/caregiver")
public class CaregiverController {

    @Autowired
     private CaregiverService caregiverService;

    @Autowired
    private UserService userService;

    @Autowired
    private PatientService patientService;


    @PostMapping(value = "/profile")
    public ResponseEntity<?> getCaregiverByUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO user = userService.findByUsername(userDTO.getUsername());
            CaregiverDTO caregiverDTO = caregiverService.findByUser(user);
            return new ResponseEntity<>(caregiverDTO , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(value = "/update")
    public ResponseEntity<?> updatePatient(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            System.out.println(caregiverDTO);
            CaregiverDTO caregiverDTONew = caregiverService.updateCaregiver(caregiverDTO);
            return new ResponseEntity<>(caregiverDTONew , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value="/myPatients")
    public ResponseEntity<?> getPatientsByCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            List<PatientDTO> patientDTOList = patientService.findPatientsByCaregiver(caregiverDTO);
            return new ResponseEntity<>(patientDTOList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(value="/myDoctor")
    public ResponseEntity<?> getMyDoctor(@RequestBody CaregiverDTO caregiverDTO) {
        try {
            DoctorDTO doctorDTO = caregiverService.findDoctorByCaregiverId(caregiverDTO.getId());
            return new ResponseEntity<>(doctorDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }


}
