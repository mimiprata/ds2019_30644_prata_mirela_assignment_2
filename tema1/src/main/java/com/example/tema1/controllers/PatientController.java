package com.example.tema1.controllers;

import com.example.tema1.dto.CaregiverDTO;
import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.dto.PatientDTO;
import com.example.tema1.dto.UserDTO;
import com.example.tema1.services.PatientService;
import com.example.tema1.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin("http://localhost:3000")
@Controller
@RequestMapping
public class PatientController {
    @Autowired
    private PatientService patientService;
    @Autowired
    private UserService userService;

    @PostMapping(value = "/patientProfile")
    public ResponseEntity<?> getPatientByUser(@RequestBody UserDTO userDTO) {
        try {
            PatientDTO patientDTONew = patientService.findByUser(userService.findByUsername(userDTO.getUsername()));
            return new ResponseEntity<>(patientDTONew , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/updatePatient")
    public ResponseEntity<?> updatePatient(@RequestBody PatientDTO patientDTO) {
        try {
            System.out.println(patientDTO);
            PatientDTO patientDTONew = patientService.updatePatient(patientDTO);
            return new ResponseEntity<>(patientDTONew , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/getMyCaregiver")
    public ResponseEntity<?> getCaregiver(@RequestBody PatientDTO patientDTO) {
        try {
            System.out.println(patientDTO);
            CaregiverDTO caregiverDTONew = patientService.findCaregiverByPatientId(patientDTO.getId());
            return new ResponseEntity<>(caregiverDTONew , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping(value = "/getMyDoctor")
    public ResponseEntity<?> getDoctor(@RequestBody PatientDTO patientDTO) {
        try {
            System.out.println(patientDTO);
            DoctorDTO doctorDTONew = patientService.findDoctorByPatientId(patientDTO.getId());
            return new ResponseEntity<>(doctorDTONew , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }







}
