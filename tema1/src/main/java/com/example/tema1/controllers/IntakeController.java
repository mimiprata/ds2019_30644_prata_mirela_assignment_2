package com.example.tema1.controllers;

import com.example.tema1.dto.IntakeDTO;
import com.example.tema1.dto.PatientDTO;
import com.example.tema1.services.IntakeService;
import com.example.tema1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
@CrossOrigin("http://localhost:3000")
@Controller
@RequestMapping
public class IntakeController {

    @Autowired
    private IntakeService intakeService;

    @Autowired
    private PatientService patientService;

    @PostMapping(value = "/getIntakes")
    public ResponseEntity<?> getIntakesByPatient(@RequestBody PatientDTO patientDTO) {
        try {

            PatientDTO patientDTONew = patientService.findById(patientDTO.getId());
            List<IntakeDTO> intakeDTOList = intakeService.findIntakesByPatient(patientDTONew);
            return new ResponseEntity<>(intakeDTOList , HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }
}
