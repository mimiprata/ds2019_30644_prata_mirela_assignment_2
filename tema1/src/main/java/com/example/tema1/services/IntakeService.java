package com.example.tema1.services;

import com.example.tema1.dto.IntakeDTO;
import com.example.tema1.dto.MedicationDTO;
import com.example.tema1.dto.PatientDTO;
import com.example.tema1.dto.mapper.IntakeMapper;
import com.example.tema1.dto.mapper.MedicationMapper;
import com.example.tema1.dto.mapper.PatientMapper;
import com.example.tema1.entities.Intake;
import com.example.tema1.entities.Medication;
import com.example.tema1.entities.Patient;
import com.example.tema1.repositories.IntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IntakeService {
    @Autowired
    private IntakeRepository intakeRepository;

    @Autowired
    private PatientService patientService;

    @Autowired
    private IntakeMapper intakeMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    MedicationService medicationService;

    @Autowired
    MedicationMapper medicationMapper;

    public void addIntake(IntakeDTO intake){
        intakeRepository.save(intakeMapper.intakeDTOtoIntake(intake));
    }

    public IntakeDTO updateIntake(IntakeDTO intake){
        Intake oldIntake = intakeRepository.findById(intake.getId()).get();
        oldIntake.setStartDate(intake.getStartDate());
        oldIntake.setEndDate(intake.getEndDate());
        oldIntake.setDetails(intake.getDetails());
        return intakeMapper.intakeToIntakeDTO(intakeRepository.save(oldIntake));
    }

    public void deleteIntake(IntakeDTO intake){
        intakeRepository.delete(intakeMapper.intakeDTOtoIntake(intake));
    }

    public List<IntakeDTO> findAllIntakes(){
        return intakeMapper.intakeListtoIntakeDTOList(intakeRepository.findAll());
    }

    public List<IntakeDTO> findIntakesByPatient(PatientDTO patient){
        return intakeMapper.intakeListtoIntakeDTOList(intakeRepository.findByPatient(patientMapper.patientDTOtoPatient(patient)));
    }

    public IntakeDTO findIntakeByMedication(MedicationDTO medication){
        return intakeMapper.intakeToIntakeDTO(intakeRepository.findByMedication(medicationMapper.medicationDTOtoMedication(medication)));
    }

    public void addInttakeToAPatient(Integer idPatient, Integer idMedication ,IntakeDTO intake) {
            PatientDTO patientDTO= patientService.findById(idPatient);
            Intake newIntake = new Intake();
            newIntake.setStartDate(intake.getStartDate());
            newIntake.setEndDate(intake.getEndDate());
            newIntake.setDetails(intake.getDetails());
            MedicationDTO medicationDTO = medicationService.findById(idMedication);
            newIntake.setMedication(medicationMapper.medicationDTOtoMedication(medicationDTO));
            newIntake.setPatient(patientMapper.patientDTOtoPatient(patientDTO));

            intakeRepository.save(newIntake);
    }

    public void updateIntakeToAPatient(Integer idPatient, Integer idMedication, IntakeDTO intake) {
        PatientDTO patientDTO= patientService.findById(idPatient);
        Intake newIntake = new Intake();
        newIntake.setId(intake.getId());
        newIntake.setStartDate(intake.getStartDate());
        newIntake.setEndDate(intake.getEndDate());
        newIntake.setDetails(intake.getDetails());
        MedicationDTO medicationDTO = medicationService.findById(idMedication);
        newIntake.setMedication(medicationMapper.medicationDTOtoMedication(medicationDTO));
        newIntake.setPatient(patientMapper.patientDTOtoPatient(patientDTO));

        intakeRepository.save(newIntake);
    }

    public void deleteIntakeById(Integer id) {
        Intake  intake= intakeRepository.findById(id).get();
        intakeRepository.delete(intake);

    }
}
