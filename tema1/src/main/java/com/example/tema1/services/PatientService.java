package com.example.tema1.services;

import com.example.tema1.dto.*;
import com.example.tema1.dto.mapper.CaregiverMapper;
import com.example.tema1.dto.mapper.DoctorMapper;
import com.example.tema1.dto.mapper.PatientMapper;
import com.example.tema1.dto.mapper.UserMapper;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Patient;
import com.example.tema1.entities.User;
import com.example.tema1.repositories.CaregiverRepository;
import com.example.tema1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private CaregiverService caregiverService;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private UserService userService;

    public void addPatient(PatientDTO patient){
        patientRepository.save(patientMapper.patientDTOtoPatient(patient));
    }

    public PatientDTO updatePatient(PatientDTO patient){
        Patient oldPatient = patientRepository.findById(patient.getId()).get();
        oldPatient.setName(patient.getName());
        oldPatient.setAddress(patient.getAddress());
        oldPatient.setBirthDate(patient.getBirthDate());
        oldPatient.setGender(patient.getGender());
        oldPatient.setMedicalRecord(patient.getMedicalRecord());
        return patientMapper.patientToPatientDTO(patientRepository.save(oldPatient));
    }

    public void deletePatient(PatientDTO patient){
        patientRepository.delete(patientMapper.patientDTOtoPatient(patient));
    }

    public List<PatientDTO> findAllPatients(){
        return patientMapper.patientListtoPatientDTOList(patientRepository.findAll());
    }

    public List<PatientDTO> findPatientsByCaregiver(CaregiverDTO caregiver){
        return patientMapper.patientListtoPatientDTOList(patientRepository.findByCaregiver(caregiverMapper.caregiverDTOtoCaregiver(caregiver)));
    }


    public PatientDTO findById(Integer id){
        return patientMapper.patientToPatientDTO(patientRepository.findById(id).get());
    }

    public PatientDTO findByUser(UserDTO user){
        return patientMapper.patientToPatientDTO(patientRepository.findByUser(userMapper.userDTOtoUser(user)));
    }

    public CaregiverDTO findCaregiverByPatientId(Integer id){
        return caregiverMapper.caregiverToCaregiverDTO(patientRepository.findById(id).get().getCaregiver());
    }

    public DoctorDTO findDoctorByPatientId(Integer id){
        CaregiverDTO caregiverDTO = findCaregiverByPatientId(id);
        return caregiverService.findDoctorByCaregiverId(caregiverDTO.getId());
    }

    public void addPatientToCaregiver(PatientDTO patient, UserDTO userDTO, Integer idCaregiver) {
        Patient newPatient = getPatient(patient, idCaregiver);
        User user = new User(userDTO.getUsername(),userDTO.getPassword(), "PATIENT");
        newPatient.setUser(user);

        patientRepository.save(newPatient);
    }

    private Patient getPatient(PatientDTO patient, Integer idCaregiver) {
        Patient newPatient = new Patient();
        newPatient.setName(patient.getName());
        newPatient.setAddress(patient.getAddress());
        newPatient.setBirthDate(patient.getBirthDate());
        newPatient.setGender(patient.getGender());
        newPatient.setMedicalRecord(patient.getMedicalRecord());
        Caregiver caregiver = caregiverRepository.findById(idCaregiver).get();
        newPatient.setCaregiver(caregiver);
        return newPatient;
    }

    public void updatePatientWithCaregiver(PatientDTO patient, Integer idCaregiver) {
        Patient oldPatient = patientRepository.findById(patient.getId()).get();
        oldPatient.setName(patient.getName());
        oldPatient.setAddress(patient.getAddress());
        oldPatient.setBirthDate(patient.getBirthDate());
        oldPatient.setGender(patient.getGender());
        oldPatient.setMedicalRecord(patient.getMedicalRecord());
        Caregiver caregiver = caregiverRepository.findById(idCaregiver).get();
        oldPatient.setCaregiver(caregiver);
        patientRepository.save(oldPatient);
    }

    public void deletePatientById(Integer id) {
        Patient patient = patientRepository.findById(id).get();
        User user = userService.findById(patient.getUser().getId());
        userService.deleteUser(userMapper.userToUserDTO(user));
        patientRepository.delete(patient);

    }
}
