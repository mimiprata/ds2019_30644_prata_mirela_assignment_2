package com.example.tema1.services;

import com.example.tema1.dto.CaregiverDTO;
import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.dto.UserDTO;
import com.example.tema1.dto.mapper.CaregiverMapper;
import com.example.tema1.dto.mapper.DoctorMapper;
import com.example.tema1.dto.mapper.UserMapper;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Doctor;
import com.example.tema1.entities.User;
import com.example.tema1.repositories.CaregiverRepository;
import com.example.tema1.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaregiverService {
    @Autowired
    private CaregiverRepository caregiverRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    public Caregiver findEntityById(Integer id){
        return this.caregiverRepository.findById(id).get();
    }

    public void addCaregiver(CaregiverDTO caregiver){
        caregiverRepository.save(caregiverMapper.caregiverDTOtoCaregiver(caregiver));
    }

    public void addCaregiverToDoctor(CaregiverDTO caregiver, UserDTO userDTO, Integer idDoctor){
        Caregiver newCaregiver = getCaregiver(caregiver, idDoctor);
        User user = new User(userDTO.getUsername(),userDTO.getPassword(), "CAREGIVER");
        newCaregiver.setUser(user);

        caregiverRepository.save(newCaregiver);
    }

    private Caregiver getCaregiver(CaregiverDTO caregiver, Integer idDoctor) {
        Caregiver newCaregiver = new Caregiver();
        newCaregiver.setName(caregiver.getName());
        newCaregiver.setAddress(caregiver.getAddress());
        newCaregiver.setBirthDate(caregiver.getBirthDate());
        newCaregiver.setGender(caregiver.getGender());
        Doctor doctor = doctorRepository.findById(idDoctor).get();
        newCaregiver.setDoctor(doctor);
        return newCaregiver;
    }

    public CaregiverDTO updateCaregiver(CaregiverDTO caregiver){
        Caregiver oldCaregiver = caregiverRepository.findById(caregiver.getId()).get();
        oldCaregiver.setName(caregiver.getName());
        oldCaregiver.setAddress(caregiver.getAddress());
        oldCaregiver.setBirthDate(caregiver.getBirthDate());
        oldCaregiver.setGender(caregiver.getGender());
        return caregiverMapper.caregiverToCaregiverDTO(caregiverRepository.save(oldCaregiver));
    }
    public CaregiverDTO updateCaregiverWithDoctor(CaregiverDTO caregiver, Integer idDoctor){
        Caregiver oldCaregiver = caregiverRepository.findById(caregiver.getId()).get();
        oldCaregiver.setName(caregiver.getName());
        oldCaregiver.setAddress(caregiver.getAddress());
        oldCaregiver.setBirthDate(caregiver.getBirthDate());
        oldCaregiver.setGender(caregiver.getGender());
        Doctor doctor = doctorRepository.findById(idDoctor).get();
        oldCaregiver.setDoctor(doctor);
        return caregiverMapper.caregiverToCaregiverDTO(caregiverRepository.save(oldCaregiver));


    }

    public void deleteCaregiver(CaregiverDTO caregiver){
        caregiverRepository.delete(caregiverMapper.caregiverDTOtoCaregiver(caregiver));
    }

    public List<CaregiverDTO> findAllCaregivers(){
        return caregiverMapper.caregiverListtoCaregiverDTOList(caregiverRepository.findAll());
    }

    public List<CaregiverDTO> findCaregiversByDoctor(DoctorDTO doctor){
        return caregiverMapper.caregiverListtoCaregiverDTOList(caregiverRepository.findByDoctor(doctorMapper.doctorDTOtoDoctor(doctor)));
    }

    public CaregiverDTO findByUser(UserDTO user){
        return caregiverMapper.caregiverToCaregiverDTO(caregiverRepository.findByUser(userMapper.userDTOtoUser(user)));
    }

    public DoctorDTO findDoctorByCaregiverId(Integer id){
        return doctorMapper.doctorToDoctorDTO(caregiverRepository.findById(id).get().getDoctor());
    }

    public void deleteCaregiverById(Integer id) {
        Caregiver caregiver = caregiverRepository.findById(id).get();
        User user = userService.findById(caregiver.getUser().getId());
        userService.deleteUser(userMapper.userToUserDTO(user));
        caregiverRepository.delete(caregiver);
    }
}
