package com.example.tema1.services;

import com.example.tema1.dto.IntakeDTO;
import com.example.tema1.dto.MedicationDTO;
import com.example.tema1.dto.mapper.MedicationMapper;
import com.example.tema1.entities.Medication;
import com.example.tema1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationService {
    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private MedicationMapper medicationMapper;

    @Autowired
    private IntakeService intakeService;

    public void addMedication(MedicationDTO medication){
        medicationRepository.save(medicationMapper.medicationDTOtoMedication(medication));
    }

    public MedicationDTO updateMedication(MedicationDTO medication){
        Medication oldMedication = medicationRepository.findById(medication.getId()).get();
        oldMedication.setName(medication.getName());
        oldMedication.setDescription(medication.getDescription());
        return medicationMapper.medicationToMedicationDTO(medicationRepository.save(oldMedication));
    }

    public void deleteMedication(MedicationDTO medication){
        medicationRepository.delete(medicationMapper.medicationDTOtoMedication(medication));
    }

    public List<MedicationDTO> findAllMedications(){
        return medicationMapper.medicationListtoMedicationDTOList(medicationRepository.findAll());
    }

    public void deleteMedicationById(Integer id) {
        Medication medication = medicationRepository.findById(id).get();
        IntakeDTO intake = intakeService.findIntakeByMedication(medicationMapper.medicationToMedicationDTO(medication));
        intakeService.deleteIntake(intake);

        medicationRepository.delete(medication);
    }

    public MedicationDTO findById(Integer id){
        return medicationMapper.medicationToMedicationDTO(medicationRepository.findById(id).get());
    }
}
