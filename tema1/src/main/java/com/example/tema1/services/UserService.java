package com.example.tema1.services;

import com.example.tema1.dto.UserDTO;
import com.example.tema1.dto.mapper.UserMapper;
import com.example.tema1.entities.User;
import com.example.tema1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(UserDTO user){
        userRepository.save(userMapper.userDTOtoUser(user));
    }

    public User updateUser(UserDTO user){
        return userRepository.save(userMapper.userDTOtoUser(user));
    }

    public void deleteUser(UserDTO user){
        userRepository.delete(userMapper.userDTOtoUser(user));
    }

    public User findById(Integer id) { return userRepository.findById(id).get();}


    public void deleteUserById(Integer id){

        User user = this.userRepository.findById(id).get();
        userRepository.delete(user);

    }

    public UserDTO findByUsername(String username){
        return userMapper.userToUserDTO(userRepository.findByUsername(username));
    }

    public List<UserDTO> findAllUsers(){
       return  userMapper.userListtoUserDTOList(userRepository.findAll());
    }

}
