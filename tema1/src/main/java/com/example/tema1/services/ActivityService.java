package com.example.tema1.services;

import com.example.tema1.dto.PatientDTO;
import com.example.tema1.dto.mapper.PatientMapper;
import com.example.tema1.entities.Activity;
import com.example.tema1.entities.Patient;
import com.example.tema1.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class ActivityService {
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private PatientMapper patientMapper;
    public void addActivityToAPatient(Timestamp start, Timestamp end, String name, PatientDTO patient){
        Activity x = new Activity(start, end, name, patientMapper.patientDTOtoPatient(patient));
        activityRepository.save(x);
    }

}
