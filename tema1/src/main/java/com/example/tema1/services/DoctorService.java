package com.example.tema1.services;

import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.dto.UserDTO;
import com.example.tema1.dto.mapper.CaregiverMapper;
import com.example.tema1.dto.mapper.DoctorMapper;
import com.example.tema1.dto.mapper.UserMapper;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.User;
import com.example.tema1.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorService {
    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CaregiverMapper caregiverMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private CaregiverService caregiverService;


    public void addDoctor(DoctorDTO doctor) {
        doctorRepository.save(doctorMapper.doctorDTOtoDoctor(doctor));
    }

    public DoctorDTO findByUser(UserDTO user) {
        return doctorMapper.doctorToDoctorDTO(doctorRepository.findByUser(userMapper.userDTOtoUser(user)));
    }

/*    public DoctorDTO updateDoctor(DoctorDTO doctor) {
        Doctor oldDoctor = doctorRepository.findById(doctor.getId()).get();
        oldDoctor.setName(patient.getName());
        oldPatient.setAddress(patient.getAddress());
        oldPatient.setBirthDate(patient.getBirthDate());
        oldPatient.setGender(patient.getGender());
        oldPatient.setMedicalRecord(patient.getMedicalRecord());
        return doctorRepository.save(doctorMapper.doctorDTOtoDoctor(doctor));

    }*/

    public void deleteDoctor(DoctorDTO doctor) {
        doctorRepository.delete(doctorMapper.doctorDTOtoDoctor(doctor));
    }

    public List<DoctorDTO> findAllDoctors() {
        return doctorMapper.doctorListtoDoctorDTOList(doctorRepository.findAll());
    }

    public void deleteCaregiver(Integer id) {
        Caregiver caregiver = this.caregiverService.findEntityById(id);
        User user = caregiver.getUser();
        this.userService.deleteUserById(user.getId());
        this.caregiverService.deleteCaregiver(this.caregiverMapper.caregiverToCaregiverDTO(caregiver));
    }
}
