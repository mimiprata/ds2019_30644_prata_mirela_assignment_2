package com.example.tema1.services;

import com.example.tema1.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {
    @Autowired
    private UserService userService;

    public UserDTO login (String username, String password){
        System.out.println(userService.findByUsername(username));
        UserDTO temp = userService.findByUsername(username);
        if (temp.getPassword().equals(password)){
            return temp;
        } else
            return null;
    }
}
