package com.example.tema1.entities;


import javax.persistence.*;

import java.sql.Date;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activity")

public class Activity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "start")
    private Timestamp start;

    @Column(name = "end")
    private Timestamp end;

    @Column(name = "activity")
    private String activity;

    @ManyToOne
    @JoinColumn( name = "patient_id")
    private Patient patient;

    public Activity() {
    }

    public Activity(Timestamp start, Timestamp end, String activity) {
        this.start = start;
        this.end = end;
        this.activity = activity;
    }

    public Activity(Timestamp start, Timestamp end, String activity, Patient patient) {
        this.start = start;
        this.end = end;
        this.activity = activity;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", start=" + start +
                ", end=" + end +
                ", activity='" + activity + '\'' +
                ", patient=" + patient +
                '}';
    }
}
