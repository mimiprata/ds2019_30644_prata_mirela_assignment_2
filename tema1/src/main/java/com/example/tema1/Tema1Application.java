package com.example.tema1;

import com.example.tema1.dto.PatientDTO;
import com.example.tema1.services.ActivityService;
import com.example.tema1.services.PatientService;
import com.github.tsohr.JSONObject;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;


@SpringBootApplication

public class Tema1Application {
    private final static String QUEUE_NAME = "mimi";
@Autowired
ActivityService activityService ;

@Autowired
SimpMessagingTemplate websocket;
@Autowired
    PatientService patientService;
    public static void main(String[] args) {

        SpringApplication.run(Tema1Application.class, args);
    }


    @Bean
    public Queue myQueue() {
        return new Queue(QUEUE_NAME, false, false, false, null);
    }

   /* @RabbitListener(queues = "sender")
    public void listen(String in) {
        System.out.println("++++"+in);
    }*/
   public Boolean sendWarning(CustomMessage customMessage)
   {
       long diff = customMessage.getEndTime().getTime()-customMessage.getStartTime().getTime();
       long diffHours = diff / (60 * 60 * 1000);
       long diffMinutes = diff / (60 * 1000);
       switch (customMessage.getActivity())
       {
           case "Sleeping":

               //System.out.println(diffHours);
               if(diffHours>9) return true;
               break;
           case "Leaving\t":
             //  System.out.println("outdoor"+diffHours);
               if(diffHours>2) return true;
               break;
           case "Toileting\t":
               //System.out.println(diffMinutes);
               if(diffMinutes>10) return true;
               break;
           default:
               return false;

       }
       return false;
   }
    @RabbitListener(queues = QUEUE_NAME)
    public void receiveMessage(final CustomMessage customMessage) {
       // System.out.println("Received message as specific class: {}"+customMessage.toString());
        /*Patient p = patientRepository.findById(Integer.parseInt(customMessage.getPatient_id())).get();
        Activity a = new Activity(customMessage.getStartTime(), customMessage.getEndTime(),customMessage.getActivity(),p);
        //System.out.println(a.toString());
        activityRepository.save(a);*/
        PatientDTO p = patientService.findById(Integer.parseInt(customMessage.getPatient_id()));
        //activityService.addActivityToAPatient(customMessage.getStartTime(),customMessage.getEndTime(),customMessage.getActivity(),p);
        if (sendWarning(customMessage)) {

            Integer idCaregiver=patientService.findCaregiverByPatientId(Integer.parseInt(customMessage.getPatient_id())).getId();
            String patientName = patientService.findById(Integer.parseInt(customMessage.getPatient_id())).getName();

            String json = new JSONObject().put("idCaregiver",idCaregiver).put("patientName",patientName).put("activity",customMessage.getActivity()).put("start",customMessage.getStartTime()).put("end",customMessage.getEndTime()).toString();
            this.websocket.convertAndSend("/topic" , json);

        }
    }
    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
