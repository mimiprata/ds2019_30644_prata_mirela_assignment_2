package com.example.tema1.dto.mapper;

import com.example.tema1.dto.UserDTO;
import com.example.tema1.dto.UserViewDTO;
import com.example.tema1.entities.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User userDTOtoUser(UserDTO userDTO);
    UserDTO userToUserDTO(User user);
    List<UserDTO> userListtoUserDTOList (List<User> users);
    List<User> userDTOListtoUserList (List<UserDTO> userDTOS);
    User userViewDTOtoUser(UserViewDTO userViewDTO);
    UserViewDTO userToUserViewDTO(User user);
}
