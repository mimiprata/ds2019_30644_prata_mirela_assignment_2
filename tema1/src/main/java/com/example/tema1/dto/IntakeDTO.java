package com.example.tema1.dto;

import com.example.tema1.entities.Medication;

import java.sql.Date;

public class IntakeDTO {
    private Integer id;
    private Date startDate;
    private Date endDate;
    private String details;
    private MedicationDTO medication;

    public IntakeDTO() {
    }

    public IntakeDTO(Integer id, Date startDate, Date endDate, String details, MedicationDTO medication) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.details = details;
        this.medication = medication;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }
}
