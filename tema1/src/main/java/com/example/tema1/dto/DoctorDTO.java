package com.example.tema1.dto;

import com.example.tema1.entities.User;

import java.util.List;

public class DoctorDTO {
    private Integer id;
    private String name;
    private String address;
    private User user;
    private List<CaregiverDTO> caregivers;

    public DoctorDTO() {
    }

    public DoctorDTO(Integer id, String name, String address, User user, List<CaregiverDTO> caregivers) {
        this.id = id;
        this.name = name;
        this.address = address;/*
        this.user = user;
        this.caregivers = caregivers;*/
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
/*
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CaregiverDTO> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<CaregiverDTO> caregivers) {
        this.caregivers = caregivers;
    }*/
}
