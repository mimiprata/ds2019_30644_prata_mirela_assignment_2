package com.example.tema1.dto.mapper;

import com.example.tema1.dto.MedicationDTO;
import com.example.tema1.dto.MedicationViewDTO;
import com.example.tema1.entities.Medication;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicationMapper {
    Medication medicationDTOtoMedication(MedicationDTO medicationDTO);
    MedicationDTO medicationToMedicationDTO(Medication medication);
    List<MedicationDTO> medicationListtoMedicationDTOList (List<Medication> medications);
    List<Medication> medicationDTOListtoMedicationList (List<MedicationDTO> medicationDTOS);
    Medication medicationViewDTOtoMedication(MedicationViewDTO medicationViewDTO);
    MedicationViewDTO medicationToMedicationViewDTO(Medication medication);
}
