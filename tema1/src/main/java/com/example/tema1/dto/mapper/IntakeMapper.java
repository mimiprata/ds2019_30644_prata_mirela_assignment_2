package com.example.tema1.dto.mapper;

import com.example.tema1.dto.IntakeDTO;
import com.example.tema1.dto.IntakeViewDTO;
import com.example.tema1.dto.MedicationDTO;
import com.example.tema1.entities.Intake;
import com.example.tema1.entities.Medication;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IntakeMapper {
    Intake intakeDTOtoIntake(IntakeDTO intakeDTO);
    IntakeDTO intakeToIntakeDTO(Intake intake);
    List<IntakeDTO> intakeListtoIntakeDTOList (List<Intake> intakes);

    Medication medicationDTOtoMedication(MedicationDTO medicationDTO);
    MedicationDTO medicationToMedicationDTO(Medication medication);

    List<Intake> intakeDTOListtoIntakeList (List<IntakeDTO> intakeDTOS);
    Intake intakeViewDTOtoIntake(IntakeViewDTO intakeViewDTO);
    IntakeViewDTO intakeToIntakeViewDTO(Intake intake);
}
