package com.example.tema1.dto.mapper;

import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.dto.DoctorViewDTO;
import com.example.tema1.entities.Doctor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DoctorMapper {
    Doctor doctorDTOtoDoctor(DoctorDTO doctorDTO);
    DoctorDTO doctorToDoctorDTO(Doctor doctor);
    List<DoctorDTO> doctorListtoDoctorDTOList (List<Doctor> doctors);
    List<Doctor> doctorDTOListtoDoctorList (List<DoctorDTO> doctorDTOS);
    Doctor doctorViewDTOtoDoctor(DoctorViewDTO doctorViewDTO);
    DoctorViewDTO doctorToDoctorViewDTO(Doctor doctor);
}
