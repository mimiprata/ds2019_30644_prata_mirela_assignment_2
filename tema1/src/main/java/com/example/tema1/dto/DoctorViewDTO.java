package com.example.tema1.dto;


import java.util.List;

public class DoctorViewDTO {

    private Integer id;
    private String name;
    private String address;
    private List<CaregiverViewDTO> caregivers;

    public DoctorViewDTO() {
    }

    public DoctorViewDTO(Integer id, String name, String address, List<CaregiverViewDTO> caregivers) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.caregivers = caregivers;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<CaregiverViewDTO> getCaregivers() {
        return caregivers;
    }

    public void setCaregivers(List<CaregiverViewDTO> caregivers) {
        this.caregivers = caregivers;
    }
}
