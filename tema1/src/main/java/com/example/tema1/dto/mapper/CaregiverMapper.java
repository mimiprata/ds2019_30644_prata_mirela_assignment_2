package com.example.tema1.dto.mapper;


import com.example.tema1.dto.CaregiverDTO;
import com.example.tema1.dto.CaregiverViewDTO;
import com.example.tema1.dto.DoctorDTO;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Doctor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CaregiverMapper {
    Caregiver caregiverDTOtoCaregiver(CaregiverDTO caregiverDTO);
    CaregiverDTO caregiverToCaregiverDTO(Caregiver caregiver);
    Doctor doctorDTOtoDoctor(DoctorDTO doctorDTO);
    DoctorDTO doctorToDoctorDTO(Doctor doctor);
    List<CaregiverDTO> caregiverListtoCaregiverDTOList (List<Caregiver> caregivers);
    List<Caregiver> caregiverDTOListtoCaregiverList (List<CaregiverDTO> caregiverDTOS);
    Caregiver caregiverViewDTOtoCaregiver(CaregiverViewDTO caregiverViewDTO);
    CaregiverViewDTO caregiverToCaregiverViewDTO(Caregiver caregiver);
}
