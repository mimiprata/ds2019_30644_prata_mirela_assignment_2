package com.example.tema1.dto.mapper;

import com.example.tema1.dto.CaregiverDTO;
import com.example.tema1.dto.PatientDTO;
import com.example.tema1.dto.PatientViewDTO;
import com.example.tema1.entities.Caregiver;
import com.example.tema1.entities.Patient;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PatientMapper {

    Patient patientDTOtoPatient(PatientDTO patientDTO);
    PatientDTO patientToPatientDTO(Patient patient);
    List<PatientDTO> patientListtoPatientDTOList (List<Patient> patients);

    Caregiver caregiverDTOtoCaregiver(CaregiverDTO caregiverDTO);
    CaregiverDTO caregiverToCaregiverDTO(Caregiver caregiver);
    List<Patient> patientDTOListtoPatientList (List<PatientDTO> patientDTOS);
    Patient patientViewDTOtoPatient(PatientViewDTO patientViewDTO);
    PatientViewDTO patientToPatientViewDTO(Patient patient);
}
