package com.example.tema1.dto;

import com.example.tema1.entities.Caregiver;

public class NewCaregiverDTO {

    private CaregiverDTO caregiverDTO;
    private UserDTO userDTO;

    public NewCaregiverDTO() {
    }

    public NewCaregiverDTO(CaregiverDTO caregiverDTO, UserDTO userDTO) {
        this.caregiverDTO = caregiverDTO;
        this.userDTO = userDTO;
    }

    public CaregiverDTO getCaregiverDTO() {
        return caregiverDTO;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiverDTO = caregiverDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
